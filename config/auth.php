<?php 

return [
	'multi' => [
        'user' => [
            'driver' => 'eloquent',
            'model'  => App\Models\User::class,
            'table'  => 'user'
        ],
        'admin' => [
            'driver' => 'eloquent',
            'model'  => App\Models\AdminUser::class,
            'table'  => 'adminuser'
        ]
     ],
];
<?php
$file = fopen('data/KUA-PPA-2016-kegiatan.csv', 'r');
$i = 0;
$keys = array();

$dbh = new PDO('mysql:host=localhost;dbname=rapbd_dki_prod;', 'root', '');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	

$id;
/** committee */
	$committee_name;
	$stmtC = $dbh->prepare('insert ignore into committee (id, name) values(:id, :name)');
	$stmtC->bindParam(':id', $id);
	$stmtC->bindParam(':name', $committee_name);
 	//

/** skpd //
	$skpd_name; $skpd_code; $skpd_committee_id;
	$stmtS = $dbh->prepare('insert ignore into skpd (id, committee_id, name, skpd_code) values(:id, :committee_id, :name, :skpd_code)');
	$stmtS->bindParam(':id', $id);
	$stmtS->bindParam(':committee_id', $skpd_committee_id);
	$stmtS->bindParam(':name', $skpd_name);
	$stmtS->bindParam(':skpd_code', $skpd_code);
	*/

/** urusan //
	$matter_num; $matter_name;
	$stmtM = $dbh->prepare('insert ignore into matter (id, num, name) values(:id, :matter_num, :matter_name)');
	$stmtM->bindParam(':id', $id);
	$stmtM->bindParam(':matter_num', $matter_num);
	$stmtM->bindParam(':matter_name', $matter_name);
	*/

/** program //
	$program_num; $program_name; $matter_id;
	$stmtP = $dbh->prepare('insert ignore into program (id, matter_id, num, name) values(:id, :matter_id, :program_num, :program_name)');
	$stmtP->bindParam(':id', $id);
	$stmtP->bindParam(':matter_id', $matter_id);
	$stmtP->bindParam(':program_num', $program_num);
	$stmtP->bindParam(':program_name', $program_name);
	*/

/** activity //
	$program_id; $skpd_id; $quantity; $unit; $activity_name; $budget_nominal;
	$stmtA = $dbh->prepare('insert ignore into activity '
		. '(id, program_id, skpd_id, name, quantity, unit, budget_nominal) '
		. 'values(:id, :program_id, :skpd_id, :activity_name, :quantity, :unit, :budget)');
	$stmtA->bindParam(':id', $id);
	$stmtA->bindParam(':program_id', $program_id); $stmtA->bindParam(':skpd_id', $skpd_id); 
	$stmtA->bindParam(':activity_name', $activity_name); 
	$stmtA->bindParam(':quantity', $quantity); $stmtA->bindParam(':unit', $unit); $stmtA->bindParam(':budget', $budget_nominal);
	*/

/** skpd_matter */
	$skpd_id; $matter_id;
	$stmtSM = $dbh->prepare('insert into skpd_matter (skpd_id, matter_id) values (:skpd_id, :matter_id)');
	$stmtSM->bindParam(':skpd_id', $skpd_id);
	$stmtSM->bindParam(':matter_id', $matter_id);
	//


$dbh->beginTransaction();
while($line = fgetcsv($file)) {
	if($i == 0) {
		$keys = $line;
		//echo '<pre>' . print_r($keys, 1) . '</pre>';
		$i++;
		continue;
	}

	$budget = array();

	$budget = $line;

	try {

		/** committee //
		$id = $budget[1];
		$committee_name = 'Komisi ' . $budget[0];
		$stmtC->execute();
		*/

		/** skpd //
		$skpd_committee_id = $budget[1];
		$id = $budget[2];
		$skpd_code = $budget[3];
		$skpd_name = $budget[4];
		$stmtS->execute();
		*/

		/** urusan //
		$id = $budget[5] * 100;
		$matter_num = $budget[5];
		$matter_name = $budget[6];
		$stmtM->execute();
		*/

		/** program //
		$matter_id = $budget[5] * 100;
		$id = str_replace('.', '', $budget[7]);
		$program_num = $budget[7];
		$program_name = $budget[8];
		$stmtP->execute();
		*/

		/** activity //
		$id = $budget[9];
		$program_id = str_replace('.', '', $budget[7]);
		$skpd_id = $budget[2];
		$quantity = $budget[13]; 
		$unit = $budget[14]; 
		$activity_name = $budget[10];
		$budget_nominal = parseInt($budget[11]);
		$stmtA->execute();
		*/

		/** skpd_matter */
		$skpd_id = $budget[2];
		$matter_id = $budget[5] * 100;
		$stmtSM->execute();	
		//

	} catch(PDOException $e) {
		exit('<pre>Line: ' . print_r($budget, 1) . '<br>Error: ' . print_r($e->getMessage(), 1) . '</pre>');
	}

	echo '<pre>' . print_r($budget, 1) . '</pre>';
	$i++;
}
$dbh->commit();

echo '<br>' . $i;

function parseInt($input) {
	$input = trim($input);

	if($input == '-') {
		return 0;
	}

	return (double) (str_replace(',', '', $input));
}


$('#date').datepicker({
  format: 'dd-mm-yyyy',
});

var sortActivities = $('#filter-activities');
var searchText = $('#order-search');
sortActivities.on('change', function() {
  if($(this).val() == 'budget_delta') {
    searchText.attr('disabled', 'disabled');
  } else {
    searchText.removeAttr('disabled');
  }
});

if(sortActivities.val() == 'budget_delta') {
  $('#order-search').attr('disabled', 'disabled');
}
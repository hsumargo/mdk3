<?php
$file = fopen('data/KUA-PPA-2016-komponen.csv', 'r');
$i = 0;
$keys = array();

$dbh = new PDO('mysql:host=localhost;dbname=rapbd_dki_prod;', 'root', '');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	

$id;

/** komponen */
// LokasiID,Komisi,SKPDID,SKPDKode,SKPDNama,UrusanKode,UrusanNama,ProgramKode,ProgramTeks,KegiatanID,KegiatanNo,Kegiatan,Vol,Satuan, Anggaran Kegiatan ,KegiatanRekID,RekeningKode,RekeningNama, NilaiAnggaran ,KomponenRid,KomponenId,KomponenNama,KomponenSatuan,KomponenHarga1,KomponenSpek,HargaSatuan,Vol,Satuan,Vol2,satuan2,Vol3,satuan3,Vol4,satuan4,Vol5,satuan5,Pajak, AnggaranKomponen

// 0,5,1001,1.01.001,DINAS PENDIDIKAN,1.01,Pendidikan,1.01.02,Program Wajib Belajar Dua Belas Tahun,19487,1,Penyediaan BIaya Operasional Pendidikan (BOP) SDLB Negeri,8,SDLB Negeri," 2,158,644,000 ",143395,5.2.1.02.02,Honorarium Pegawai Honorer/Tidak Tetap," 100,800,000 ",11547,1.09.26.05.01.008.009,Honorarium Pegawai Honorer/ Tidak Tetap (Bop Sdlb),Orang/ Bulan,400000,,400000,21,Orang,12,bln,NULL,NULL,NULL,NULL,NULL,NULL,0," 100,800,000 "
	
	$s = $dbh->prepare(
		'insert into component (activity_id, component_code, component_tax, component_name, component_spec, component_budget_per_unit, component_unit, component_budget, vol1, unit1, vol2, unit2, vol3, unit3, vol4, unit4, vol5, unit5)
		values (:activity_id, :component_code, :component_tax, :component_name, :component_spec, :component_budget_per_unit, :component_unit, :component_budget, :vol1, :unit1, :vol2, :unit2, :vol3, :unit3, :vol4, :unit4, :vol5, :unit5)');


$dbh->beginTransaction();
while($line = fgetcsv($file)) {
	if($i == 0) {
		$keys = $line;
		echo '<pre>' . print_r($keys, 1) . '</pre>';
		$i++;
		continue;
	}

	$budget = array();

	$budget = $line;

	try {
		$s->bindValue(':activity_id', $budget[9]);
		$s->bindValue(':component_code', $budget[20]);
		$s->bindValue(':component_name', $budget[21]);
		$s->bindValue(':component_spec', $budget[24]);
		$s->bindValue(':component_budget_per_unit', parseInt($budget[23]));
		$s->bindValue(':component_unit', $budget[22]);
		$s->bindValue(':component_budget', parseInt($budget[37]));
		$s->bindValue(':component_tax', parseInt($budget[36]));
		$s->bindValue(':vol1', $budget[26]); $s->bindValue(':unit1', $budget[27]);
		$s->bindValue(':vol2', $budget[28]); $s->bindValue(':unit2', $budget[29]);
		$s->bindValue(':vol3', $budget[30]); $s->bindValue(':unit3', $budget[31]);
		$s->bindValue(':vol4', $budget[32]); $s->bindValue(':unit4', $budget[33]);
		$s->bindValue(':vol5', $budget[34]); $s->bindValue(':unit5', $budget[35]);
		$s->execute();
	} catch(PDOException $e) {
		exit('<pre>Line: ' . print_r($budget, 1) . '<br>Error: ' . print_r($e->getMessage(), 1) . '</pre>');
	}

	//echo '<pre>' . print_r($budget, 1) . '</pre>';
	$i++;
}
$dbh->commit();

echo '<br>' . $i;

function parseInt($input) {
	$input = trim($input);

	if($input == '-') {
		return 0;
	}

	return (double) (str_replace(',', '', $input));
}


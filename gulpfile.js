var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles([
        'bower_components/adminLTE/dist/css/AdminLTE.min.css', 
        'bower_components/adminLTE/dist/css/skins/skin-blue.min.css',
        'css/default.css', 'css/public.css', 'css/admin.css'
    ], 'public/build/css/styles.css', 'public');

    mix.styles([
        'bower_components/adminLTE/dist/css/AdminLTE.min.css', 
        'bower_components/adminLTE/dist/css/skins/skin-blue.min.css',
        'css/default.css', 'css/budgetBrowser.css', 
    ], 'public/build/css/budgetBrowser.css', 'public');

    mix.scripts([
        'plugins/jQuery/jQuery-2.1.4.min.js', 'bootstrap/js/bootstrap.min.js', 'dist/js/app.min.js',
        'plugins/datatables/jquery.dataTables.min.js', 'plugins/datatables/dataTables.bootstrap.min.js',
        'plugins/slimScroll/jquery.slimscroll.min.js', 'plugins/fastclick/fastclick.js',
        'plugins/datepicker/bootstrap-datepicker.js', 'dist/js/mitra.js'
    ], 'public/build/js/scripts.js', 'public/bower_components/adminLTE')
});
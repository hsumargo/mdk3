<footer class="main-footer">
    <!-- To the right -->
    <!-- <div class="pull-right hidden-xs">
        Anything you want
    </div> -->
    <!-- Default to the left -->
    <strong>Copyright © {{ date('Y') }} <a href="#">Kawal APBD</a>.</strong> All rights reserved.
</footer>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-71369792-1', 'auto');
  ga('send', 'pageview');

</script>
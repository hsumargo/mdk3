@extends('dashboard')

@section('opengraph')
  <meta property="fb:app_id" content="1033988359974181" /> 
  <meta property="og:type"   content="kawal-apbd:activity" /> 
  <meta property="og:url"    content="{{ Request::url() }}" /> 
  <meta property="og:title"  content="{{ $activity->name }}" /> 
  <meta property="og:image"  content="https://fbstatic-a.akamaihd.net/images/devsite/attachment_blank.png" /> 
@endsection

@section('content')
    <div class="row activity-body">
        <div class="col-xs-12">
            <h3>{{ $activity->name }}</h3>
            <h4 class="skpd-name">SKPD: {{ $activity->skpd->name }}</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h3>Anggaran Total: {{ Helper::rupiah_format($activity->budget_nominal) }}</h3>
        </div>
    </div>

    <div class="row budgets-details">
        <div class="col-md-12"><strong>PERINCIAN (dalam rupiah)</strong></div>
        @foreach($activity->komponens as $komponen)
            <div class="col-md-4 col-xs-6">
                {{ $komponen->component_name }} {{ $komponen->component_spec }}
            </div>
            <div class="col-md-3 col-xs-6 text-right"> 
                <strong>{{ number_format(floatval($komponen->component_budget), 2) }}</strong><br>
            </div>
            <div class="col-xs-12" style="margin-bottom:15px">
                {{ Helper::displayPriceDetails($komponen) }}
            </div>  
        @endforeach
    </div>

    <hr>

    <div class="row">
        <div class="col-md-4 col-xs-6">
            <div class="action-cta">Perincian janggal?</div>
            <div class="action-wrap flag-count-wrap @if($isFlagged) active @endif" data-id="{{$activity->id}}">
                <span class="flag-count-label action-label">
                    Flag
                </span>
                <div class="flag-count">
                   {!! Helper::shortenNumber($activity->flags()->count()) !!}
                </div>
            </div>
        </div>

        <div class="col-md-4 col-xs-6">
            <div class="action-cta">Bagikan ke Facebook?</div>
            <a class="action-wrap share-wrap" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::url()) }}&amp;display=popup" target="_blank">
                <span class="action-label share-label">Bagikan!</span>
            </a>
        </div>

        <div class="col-xs-12 facebook-comments-wrap">
            <div class="box-body table-responsive">
                <div class="fb-like" data-href="{{ Request::url() }}" data-layout="standard" data-action="like" data-show-faces="true"></div>
                <div class="fb-comments" data-href="{{ Request::url() }}" data-numposts="5" data-width="100%" notify="true">
                </div>
            </div>

            <div id="fb-root"></div>
        </div>
    </div>

    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Untuk menggunakan fitur ini, login dengan Facebook diperlukan</h4>
          </div>
          <!-- <div class="modal-body">
            ...
          </div> -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {!! Html::link(url('/facebook/login'), 'Login dengan Facebook', array('class' => 'btn btn-primary')) !!}
          </div>
        </div>
      </div>
    </div>
@endsection

@section('javascripts')
    <script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1033988359974181',
            xfbml      : true,
            version    : 'v2.5'
        });

        var commentCallback = function(response) {
            $.get('{{ url('/v1/activity/recalculateComments/' . $activity->id) }}');
        };

        FB.Event.subscribe('comment.create', commentCallback);
        FB.Event.subscribe('comment.remove', commentCallback);
    };

    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=1033988359974181";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    $('.flag-count-wrap').click(function() {
        $_token = "{{ csrf_token() }}";
        var $tapped = $(this);
        var flag = true;

        if($tapped.hasClass('active')) {
            flag = false;
        }
        
        $.post('{{url('/flagPost')}}/' + $(this).data('id'), {'_token': $_token, 'flag':flag}, function(data) {
            if(data.error) {
                $('#login-modal').modal();
            }

            if(data.success) {
                if($tapped.hasClass('active')) {
                    $tapped.removeClass('active');
                } else {
                    $tapped.addClass('active');
                }

                $('.flag-count-wrap .diff-nominal').text(data.flag_count);
            }
        });
    });

    </script>
@endsection

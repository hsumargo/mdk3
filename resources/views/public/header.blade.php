<header class="header-main">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="{{url('/')}}" class="navbar-brand">KUKi</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <div class="collapse navbar-collapse pull-right" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="{{url('/')}}">Home</a></li>
            <li class="active"><a href="{{url('/dashboard')}}">Dashboard <span class="sr-only">(current)</span></a></li>
          </ul>
          <form class="navbar-form navbar-left" role="search">


            @if(!Request::is('public/activities/*'))
              <div class="form-group">
                <div class="input-group"> 
                  <input type="text" class="form-control" value="{{Input::get('search')}}" name="search" id="inputGroupSuccess1" aria-describedby="inputGroupSuccess1Status"> 
                  <span class="input-group-addon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span> 
                </div>
              </div>
            @endif
          </form>
        </div>

      </div>
      
    </nav>
  </header>
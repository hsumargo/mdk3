@extends('public')

@section('content')
    
    <p>Sumber data: <a target="_blank" href="http://data.jakarta.go.id/dataset/data-perencanaan-apbd-2016/resource/e45cb92a-26e9-4cfe-8694-bde8f6f5ef6b">KUA - PPAS DKI Jakarta 2016</a></p>
    <p>Aplikasi ini bersifat open dan tidak memihak pada organisasi mana pun.</p>

    {!! $activities->appends(['search' => Input::get('search')])->render() !!}
    @foreach($activities as $activity)
        <div class="row budget-row">
            <div class="col-xs-4 col-sm-3 col-lg-3 ">
                <div class="row">
                    <div class="col-md-6 left-counts">
                        <div class="budget-diff-wrap @if($activity->budget_delta < 0) favor-dprd @elseif($activity->budget_delta > 0) favor-pemprov @endif">
                            <div class="vollkorn">
                                Selisih
                            </div>
                            <div>
                                {!! Helper::shortenNumber($activity->budget_delta) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 right-counts">
                        <div class="flag-count-wrap @if(Auth::check() && !empty($flags[$activity->id])) active @endif" data-id="{{$activity->id}}">
                            <span>
                                {!! Helper::shortenNumber($activity->flag_count) !!}
                            </span>
                        </div>
                        <div class="comment-count-wrap">
                            <span>
                                <span class="fb-comments-count" data-href="{{ url('/activities/' . $activity->id) }}">...</span>
                            </span>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xs-8 col-sm-9 col-lg-9 main-data">
                <div>{{ $activity->name }}</div>
                <div>{{ $activity->skpd_name }}</div>
                <div class="row">
                    <div class="col-md-6"><span class="vollkorn">Versi 1</span><div>Rp. {{ number_format($activity->budget_banggar, 2) }}</div></div>
                    <div class="col-md-6"><span class="vollkorn">Versi 2</span><div>Rp. {{ number_format($activity->budget_final, 2) }}</div></div>
                </div>
                <div>{!! Html::link('/activities/' . $activity->id, 'Lihat data selengkapnya') !!}</div>
            </div>
        </div>
    @endforeach

    {!! $activities->appends(['search' => Input::get('search')])->render() !!}

    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Untuk menggunakan fitur ini, login dengan Facebook diperlukan</h4>
          </div>
          <!-- <div class="modal-body">
            ...
          </div> -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <a href="/facebook/login" class="btn btn-primary">Login dengan Facebook</a>
          </div>
        </div>
      </div>
    </div>
    
@endsection

@section('javascripts')
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=1033988359974181";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    $('.flag-count-wrap').click(function() {
        $_token = "{{ csrf_token() }}";
        var $tapped = $(this);
        var flag = true;

        if($tapped.hasClass('active')) {
            flag = false;
        }
        
        $.post('/flagPost/' + $(this).data('id'), {'_token': $_token, 'flag':flag}, function(data) {
            if(data.error) {
                $('#login-modal').modal();
            }

            if(data.success) {
                if($tapped.hasClass('active')) {
                    $tapped.removeClass('active');
                } else {
                    $tapped.addClass('active');
                }
            }
        });
    });

    </script> 
@endsection

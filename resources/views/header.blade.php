<!-- Main Header -->
<header class="main-header">

  <!-- Logo -->
  <a href="{{ url('/') }}" class="logo"><b>Kawal APBD</b></a>

  <!-- Header Navbar -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    @if(Auth::user('admin'))
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    @endif
  <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav"> 
        <li class="@if(Request::url() == url('/')){{ 'active' }}@endif">{!! Html::link(url('/'), 'Home') !!}</li>
        <li class="@if(Request::is('budgetBrowser*')){{ 'active' }}@endif">{!! Html::link(url('/budgetBrowser'), 'Alokasi Anggaran') !!}</li>
        <li class="@if(Request::is('faq*')){{ 'active' }}@endif">{!! Html::link(url('/faq'), 'FAQ') !!}</li>       
        <!-- User Account Menu -->
        <li class="dropdown user user-menu">
          <!-- Menu Toggle Button -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">

            <!-- The user image in the navbar-->
            <!-- <img src="{{ asset("/bower_components/adminLTE/dist/img/user2-160x160.jpg") }}" class="user-image" alt="User Image"/> -->
            <!-- hidden-xs hides the username on small devices so only the image appears. -->
            <span ></span>
          </a>
          <ul class="dropdown-menu">

            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">Ganti Password</a>
              </div>
              <div class="pull-right">
                <a href="/logout" class="btn btn-default btn-flat">Log out</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>
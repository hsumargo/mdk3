<div class="box-body">
    <form class="form-inline" id="activities-concern-form">
        <div class="form-group">
            Cari:
        </div>
        <div class="form-group">
            <input type="text" name="keyword" class="form-control col-md-3" id="order-search" placeholder="" value="{{ Input::get('keyword') }}">
        </div>
        <div class="form-group">
            Filter By:
        </div>
        <div class="form-group">
            {!! Form::select('filterBy', $filterBy, Input::get('filterBy'), array('class' => 'form-control col-md-2', 'id' => 'filter-activities')); !!}
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>

        <button type="submit" class="btn btn-info" data-sort="showall" id="showall">Tampilkan Smua</button>
    </form>             
</div>

<table class="table table-bordered table-striped">
    <colgroup>
        <col style="width: 26%"></col>
        <col style="width: 26%"></col>
        <col style="width: 17%"></col>
        <col style="width: 17%"></col>
    </colgroup>
    <tr>
        @foreach($tableHeaders as $key => $header)
        <th>
            {{ $key }}
            @if($header == $sortColumn)
                @if($orderColumn == 'asc')
                <a href="#" data-sort="asc" data-column="{{$key}}" class="glyphicon glyphicon-triangle-bottom activities-concern-sort" aria-hidden="true"></a>
                @elseif($orderColumn == 'desc')
                <a href="#" data-sort="desc" data-column="{{$key}}" class="glyphicon glyphicon-triangle-top activities-concern-sort" aria-hidden="true"></a>
                @endif
            @else
                <a href="#" data-sort="asc" data-column="{{$key}}" class="fa fa-sort activities-concern-sort" aria-hidden="true"></a>
            @endif 

            @if($key == 'Nama Kegiatan')
                | (Komentar 
                @if($sortColumn == 'comment_count')
                    @if($orderColumn == 'asc')
                    <a href="#" data-sort="asc" data-column="comment_count" class="glyphicon glyphicon-triangle-bottom activities-concern-sort" aria-hidden="true"></a>
                    @elseif($orderColumn == 'desc')
                    <a href="#" data-sort="desc" data-column="comment_count" class="glyphicon glyphicon-triangle-top activities-concern-sort" aria-hidden="true"></a>
                    @endif
                @else
                    <a href="#" data-sort="desc" data-column="comment_count" class="fa fa-sort activities-concern-sort" aria-hidden="true"></a>
                @endif 

                , Flag 
                @if($sortColumn == 'flag_count')
                    @if($orderColumn == 'asc')
                    <a href="#" data-sort="asc" data-column="flag_count" class="glyphicon glyphicon-triangle-bottom activities-concern-sort" aria-hidden="true"></a>
                    @elseif($orderColumn == 'desc')
                    <a href="#" data-sort="desc" data-column="flag_count" class="glyphicon glyphicon-triangle-top activities-concern-sort" aria-hidden="true"></a>
                    @endif
                @else
                    <a href="#" data-sort="desc" data-column="flag_count" class="fa fa-sort activities-concern-sort" aria-hidden="true"></a>
                @endif
                )
            @endif
        </th>
        @endforeach
    </tr>

    @foreach($activities as $activity)
        <tr>
            <td>
                {!! Html::link('/activities/' . $activity->id, $activity->name, array('target' => '_blank')) !!}
                <div class="activity-stat">{{ $activity->comment_count }} Komentar, {{ $activity->flag_count }} Flag</div>
            </td>
            <td>
                {{ $activity->skpd_name }}
            </td>
            <td>
                Rp. {{ number_format($activity->budget_nominal) }}
            </td>
            <td>
                {{ $activity->nama_urusan }}
            </td>
        </tr>
    @endforeach
</table>
{!! $activities->appends(['sort' => Request::input('sort'), 'order' => Request::input('order'), 'orderBy' => Request::input('orderBy')])->render() !!}
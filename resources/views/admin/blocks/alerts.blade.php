@if ( Session::get('errors') )
  @if(is_array(Session::get('errors')))
    <div class="alert alert-danger">
        @foreach(Session::get('errors') as $value)
          <p>{{$value}}</p>
        @endforeach
    </div>
  @else
    <div class="alert alert-danger">{{ Session::get('errors') }}</div>
  @endif
@endif

@if ( Session::get('success') )
	<div class="alert alert-success">{{ Session::get('success') }}</div>
@endif

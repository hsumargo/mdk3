@extends('dashboard');

@section('content')
    <ul>
    @foreach($committees as $committee)
        <li>{!! Html::link('/committees/' . $committee->id . '/programs', $committee->name) !!}</li>
    @endforeach
    </ul>
@endsection
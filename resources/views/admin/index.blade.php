@extends('dashboard')

@section('opengraph')
    <meta property="fb:app_id" content="1033988359974181" />
@endsection

@section('content')
<div class="row" id="charts-wrap">
    <div class="col-xs-12">
        <h2>APBD untuk tahun anggaran 2016</h2>
        <hr>

        <section id="chart-title-wrap">
            <strong id="chart-title">Persentase anggaran per urusan dari total APBD.</strong><br>
            <a href="#" id="refreshConcernChart"><span class="glyphicon glyphicon-refresh"></span>  Refresh Chart</a>
        </section>
        
        <div class="chart-wrap">
            <div id="pie-chart-concern"></div>
        </div>

        <hr>

        <div id="concern-activities">

        </div>

    </div>
</div>
@endsection

@section('javascripts')
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart", "table"], 'language':'id'});
    google.setOnLoadCallback(refreshConcernPieChart);

    var groupByGlobal = 'skpd.id';
    var groupByConcernGlobal = 'urusan';
    var globalSelectionWord = '';
    var globalPage = 1;

    function refreshConcernPieChart() {
        refreshConcernData(globalSelectionWord);
        refreshConcernActivities(globalSelectionWord);
    }
    
    var defaultChartTitleText = $('#chart-title').text();

    $(window).resize(function() {
        refreshConcernData(globalSelectionWord);
    });

    $('#refreshConcernChart').on('click', function(e) {
        groupByConcernGlobal = 'urusan';
        $('input[name="filterData"]').val('');
        $('input[name="keyword"]').val('');

        $('#chart-title').text(defaultChartTitleText);

        refreshConcernPieChart();
        
        e.preventDefault();
        return false;
    })

    function drawPieChartConcern(data) {
        
        var data = google.visualization.arrayToDataTable(data);

        var formatter = new google.visualization.NumberFormat({pattern: '#,###', prefix: 'Rp '});
        formatter.format(data, 1);

        var options = {
            bar: {groupWidth: "15%"},
            height:400,
            chartArea: {  width: "90%", height: "90%" },
            tooltip:{isHtml:true, trigger: 'focus'},
            legend: {'position': 'right'},
            focusTarget: 'category',
            vAxis:{
                format:'short'
            },
            tooltip: {isHtml: true},
            allowHtml: true
        };

        var chart = new google.visualization.PieChart(document.getElementById('pie-chart-concern'));

        function selectEvent() {
            var selectedItem = chart.getSelection()[0];
            if (selectedItem) {
                var topping = data.getValue(selectedItem.row, 0);

                globalSelectionWord = topping;

                if(groupByConcernGlobal == '') {
                    return;
                }

                var chartTitleText = '';
                if(groupByConcernGlobal == 'urusan') {
                    chartTitleText = 'Presentase anggaran tiap SKPD dari anggaran ' + topping;
                    groupByConcernGlobal = 'nama skpd';
                } else if(groupByConcernGlobal == 'nama skpd') {
                    chartTitleText = 'Presentase anggaran tiap Program dari anggaran SKPD ' + topping;
                    groupByConcernGlobal = 'program';
                } else if(groupByConcernGlobal == 'program') {
                    groupByConcernGlobal = '';
                }

                if(chartTitleText != '') {
                    $('#chart-title').text(chartTitleText);   
                }

                refreshConcernData(topping);
                refreshConcernActivities(topping);

            }
        }

        google.visualization.events.addListener(chart, 'select', selectEvent);

        chart.draw(data, options);
    }

    function getConcernActivities($this) {
        var order = $this.data('sort') || getParameterByName($this.find('a').attr('href'), 'order');
        var orderBy = $this.data('column') || getParameterByName($this.find('a').attr('href'), 'orderBy');

        $.get("{{ url('/concernActivities')}}", {
            'groupBy':groupByConcernGlobal, 
            'filterData': $('input[name="filterData"]').val(),
            'filter': globalSelectionWord,
            'filterBy' : $('select[name="filterBy"]').val(),
            'keyword' : $('input[name="keyword"]').val(),
            'order' : order,
            'orderBy' : orderBy,
            'page' : $this.text(),
        }, function(data) {
            $('#concern-activities').html(data);
            rebindActivitiesForm();
        });
    }

    function getParameterByName(url, name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(url);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function rebindActivitiesForm() {

        $('#activities-concern-form').on('submit', function(e) {
            getConcernActivities($(this));
            refreshConcernData();
            e.preventDefault();
            return false;
        });
    }

    function refreshConcernActivities(filter) {
        //set default
        filter = filter || '';

        $.get('{{url('/concernActivities')}}', {
                'groupBy':groupByConcernGlobal, 
                'filterData': $('input[name="filterData"]').val(),
                'filter': filter,
                'filterBy' : $('select[name="filterBy"]').val(),
                'keyword' : $('input[name="keyword"]').val(),
                'page' : globalPage,
        }, function(data) {
            $('#concern-activities').html(data);
            rebindActivitiesForm();
        });
    }

    function refreshConcernData(filter, callback) {
        //set default
        filter = filter || '';

        //it has already deep down
        if(groupByConcernGlobal == '') {
            return;
        }

        if(filter == '') {
            groupByConcernGlobal = 'urusan';
        }

        var url = "{{ url('/v1/concern') }}";
        $.get(url, {
                'groupBy':groupByConcernGlobal, 
                'filterData': $('input[name="filterData"]').val(),
                'filter': filter,
                'filterBy' : $('select[name="filterBy"]').val(),
                'keyword' : $('input[name="keyword"]').val()
        }, function(data) {
            drawPieChartConcern(data);

            if(typeof(callback) == 'function') {
                callback();
            }
        });
    }

    //Pagination ajax
    $('#concern-activities').on('click', '.activities-concern-sort, #showall, .pagination > li', function(e) {
        if($(this).prop('id') == 'showall') {
            $('input[name="keyword"]').val('');
        }

        globalPage = parseInt($(this).text());

        getConcernActivities($(this));
        refreshConcernData(globalSelectionWord, function() {
            //Animate to top of table on new data loaded
            $('html, body').animate({
                scrollTop: $("#concern-activities").offset().top
            }, 200);
        });

        e.preventDefault();
        return false;
    });



    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=1033988359974181";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


</script>
@endsection
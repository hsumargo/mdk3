@extends('dashboard')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">

            <div class="box-body">
                <form class="form-inline">
                    <div class="form-group">
                        Cari:
                    </div>
                    <div class="form-group">
                        <input type="text" name="keyword" class="form-control col-md-3" id="order-search" placeholder="" value="{{ Input::get('keyword') }}">
                    </div>
                    <div class="form-group">
                        {!! Form::select('sort', $sort, Input::get('sort'), array('class' => 'form-control col-md-2', 'id' => 'sort-activities')); !!}
                    </div>
                    <div class="form-group">
                        {!! Form::select('order', $order, Input::get('order'), array('class' => 'form-control col-md-2', 'id' => 'order')); !!}
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="activities" class="btn btn-info">Tampilkan Smua</a>
                </form>             
            </div>

            <div class="box-body table-responsive">
                <table class="table table-bordered table-striped">
                    <colgroup>
                        <col style="width: 33%"></col>
                        <col style="width: 33%"></col>
                        <col style="width: 33%;"></col>
                    </colgroup>
                    <tr>
                        @foreach($tableHeaders as $key => $header)
                        <th>
                            {{ $key }}
                            @if($key == Request::input('sort'))
                                @if(Request::input('order') == 'asc')
                                <span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>
                                @else
                                <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                                @endif
                            @endif        
                        </th>
                        @endforeach
                    </tr>

                    @foreach($activities as $activity)
                        <tr>
                            <td>
                                {{ $activity->name }}
                            </td>
                            <td>
                                {{ $activity->skpd_name }}
                            </td>
                            <td>
                                Rp. {{ number_format($activity->budget_nominal) }}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
{!! $activities->appends(['sort' => Request::input('sort'), 'order' => Request::input('order')])->render() !!}
@endsection
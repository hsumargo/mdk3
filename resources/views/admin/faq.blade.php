@extends('dashboard')

@section('content')

<div>

    <!-- Main content -->
    <div class="content body">

		<section id="faq">
			<h2 class="page-header"><a href="#faq">FAQ</a></h2>

			<section class="q-wrap">
				<h3>Apa hubungan website ini dengan website Kawal APBD sebelumnya?</h3>
				Website ini adalah lanjutan dari Kawal APBD yang terdahulu, dapat diakses di <a href="http://rapbd-dki.kawalapbd.org/">http://rapbd-dki.kawalapbd.org/</a>
			</section>

			<section class="q-wrap">
				<h3>Berasal dari mana sumber data yang dipakai oleh website ini?</h3>
				Sumber data yang dipakai berasal dari 	
				<ul>
					<li><a target="_blank" href="http://data.jakarta.go.id/dataset/data-perencanaan-apbd-2016/resource/9e587b6e-dd7b-4457-ab34-5231620d7a8f">KUA - PPA DKI Jakarta 2016 - Kegiatan</a></li>
					<li><a target="_blank" href="http://data.jakarta.go.id/dataset/data-perencanaan-apbd-2016/resource/88dca8be-bdb9-4fbe-b040-f0abdd40d374">KUA - PPA DKI Jakarta 2016 - Komponen</a></li>
				</ul>
			</section>

			<section class="q-wrap">
				<h3>Apakah website ini berada di bawah naungan institusi tertentu?</h3>	
				Aplikasi ini bersifat open dan tidak memihak pada organisasi mana pun.
			</section>

			<section class="q-wrap">
				<h3>Apa gunanya menekan ikon ini: {!! Html::image('/img/flag_faq.png') !!}?</h3>
				Itu adalah fitur "Flag", silahkan dipakai untuk menandai kegiatan yang dirasa tidak masuk akal atau janggal. 
				Pemprov secara internal memiliki konsol untuk melihat semua kegiatan yang telah di "Flag", sehingga semua kegiatan yang dinilai oleh komunitas
				tidak masuk akal dapat ditindaklanjuti.
			</section>

			<section class="q-wrap">

				<h3>Bagaimana awal / sejarah website Kawal APBD (baik yang pertama maupun yang sekarang)?</h3>

				Website Kawal APBD jilid 2 berawal dari inisiatif beberapa Diaspora Indonesia di Singapura (
				<a href="https://www.facebook.com/wo.and" target="_blank">Andreas Wong</a>, 
				<a href="https://www.facebook.com/henrysumargo" target="_blank">Henry Sumargo</a>
				dan 
				<a href="https://www.facebook.com/liang.sier" target="_blank">Alvin Herawan</a>)
				Hackathon Merdeka 3.0, website ini masih terus ditindaklanjuti dan dikembangkan.
				<br><br>

				Sumber data berasal dari portal open data Jakarta:<br>
				<a href="http://data.jakarta.go.id/dataset/data-perencanaan-apbd-2016">
					data.jakarta.go.id/dataset/data-perencanaan-apbd-2016
				</a>
				<br><br>

				Fokus utama kali ini sedikit berbeda dengan yang website Kawal APBD pertama. Tahun ini, data mengenai anggaran sudah tersedia sampai level komponen.
				<br><br>

				Yang menjadi prioritas adalah agar publik sadar mengenai persentase alokasi anggaran dalam berbagai bidang, dan kemudian dapat melihat detail anggaran tersebut sampai level kegiatan dan komponen.
				<br><br>

				Publik juga dapat bersumbangsih dengan memberikan komentar dan flagging (untuk melakukan ini perlu login melalui akun Facebook).
				<br><br>

				Source code dan dokumen mengenai website ini akan dibuka kepada publik. Sehingga harapannya adalah agar aplikasi ini bisa terus dikembangkan. Juga agar website atau aplikasi semacam ini bisa diterapkan dan memberikan manfaat untuk instansi yang lain.

				<div class="row">
					<div class="col-sm-6">
						<h4 style="color:#c52e26">
							Kawal APBD yang pertama dibuat oleh 
							<a href="https://www.facebook.com/ainunnajib" target="_blank">Ainun Najib</a> dan 
							<a href="https://www.facebook.com/pahlevi.fikri.auliya" target="_blank">Pahlevi Fikri Auliya</a>.
						</h4>

						Lanjutan inisiatif sebelumnya, yaitu kawalpemilu.org, yang telah menuai banyak sorotan dan dukungan karena berhasil berperan sebagai pembanding data hasil Pemilu 2014 lalu.
						<br><br>

						Alamat Kawal APBD yang pertama adalah: <a href="http://rapbd-dki.kawalapbd.org">rapbd-dki.kawalapbd.org</a>.
						<br><br>

						Fokus utama adalah memberikan perbandingan dua versi Rancangan Anggaran Pendapatan dan Belanja Daerah (RAPBD) DKI Jakarta, yakni versi DPRD yang dibandingkan dengan versi e-budgeting Pemerintah Provinsi (Pemprov) DKI.
					</div>
					<div class="col-sm-6">
						<h4 style="color:#c52e26">Tentang Hackathon Merdeka:</h4>

						Hackathon Merdeka merupakan sebuah inisiatif untuk menginisiasi gerakan besar Code4Nation sebagai salah satu bentuk pemanfaatan teknologi informasi guna memecahkan permasalahan nasional.
						<br><br>

						Acara Hackathon Merdeka 3.0 dilaksanakan oleh Code4Nation dan bekerjasama dengan KPK dan ICW. Hari pelaksanaan adalah tanggal 5 & 6 Desember, dan dalam rangka menyambut Hari Anti Korupsi Internasional (9 Desember). Tema besar Hackathon Merdeka 3.0 adalah mengenai transparansi.
					</div>
				</div>
			</section>
		</section>

	</div>
</div>




@endsection

@section('javascripts')
@endsection
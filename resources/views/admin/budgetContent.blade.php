<div class="col-md-offset-2 col-md-8 budget-rows">
    <div id="main-header">
        <header>{{ $pageTitle }}</header>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <button class="btn btn-default" id="sort-alpha-desc">
                <span class="glyphicon glyphicon-sort-by-alphabet-alt" aria-hidden="true"></span>
            </button>
            <button class="btn btn-default" id="sort-alpha-asc">
                <span class="glyphicon glyphicon-sort-by-alphabet" aria-hidden="true"></span>
            </button>
        </div>
        <div class="col-xs-6">
            <button class="btn btn-default" id="sort-budget-desc">
                <span class="glyphicon glyphicon-sort-by-attributes-alt" aria-hidden="true"></span>
            </button>
            <button class="btn btn-default" id="sort-budget-asc">
                <span class="glyphicon glyphicon-sort-by-attributes" aria-hidden="true"></span>
            </button>
        </div>
    </div>
    <div class="row total-row">
        <div class="col-xs-5 col-sm-6" id="total-label">
            Total
        </div>
        <div class="col-xs-7 col-sm-6">
            <span>{{ number_format($budgetTotal) }}</span><br>
            <small>(dalam rupiah)</small>
        </div>
    </div>
@foreach($objects as $i => $object)
    <div class="row" data-budget="{{ $object->budget }}">
        <div class="col-xs-5 col-sm-6">
            <a href="{{ url('/budgetBrowser/' . $objectName . '/' . $object->id) }}">{{ $object->name }}</a>
        </div>
        <div class="col-xs-7 col-sm-6">
            <span>{{ number_format($object->budget) }}</span><br>
            <div class="bar" style="width: {{ $object->budget / $budgetMax * 100 }}%"></div>
        </div>
    </div>
@endforeach
</div>
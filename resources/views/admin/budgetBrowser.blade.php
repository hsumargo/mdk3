@extends('dashboard')

@section('stylesheet', asset('/build/css/budgetBrowser.css'))

@section('content')
<div class="row" id="main-content">
    @include('admin.budgetContent')
</div>
@endsection

@section('javascripts')
<script type="text/javascript">
$('#main-content').on('click', '#sort-budget-asc', function(e) {
    addHash($(this).prop('id'));
    $('div[data-budget]').sort(function (a, b) {
        var budgetA = parseInt($(a).data('budget'));
        var budgetB = parseInt($(b).data('budget'));
        return budgetA < budgetB ? -1 : (budgetA > budgetB) ? 1 : 0;
    }).each(function (_, container) {
      $(container).parent().append(container);
    });
});

$('#main-content').on('click', '#sort-budget-desc', function(e) {
    addHash($(this).prop('id'));
    $('div[data-budget]').sort(function (a, b) {
        var budgetA = parseInt($(a).data('budget'));
        var budgetB = parseInt($(b).data('budget'));
        return budgetA < budgetB ? 1 : (budgetA > budgetB) ? -1 : 0;
    }).each(function (_, container) {
      $(container).parent().append(container);
    });
});

$('#main-content').on('click', '#sort-alpha-asc', function(e) {
    addHash($(this).prop('id'));
    $('div[data-budget] > div:first-child').sort(function (a, b) {
        var textA = $(a).find('a').text();
        var textB = $(b).find('a').text();
        return textA.localeCompare(textB);
    }).map(function() {
        return $(this).closest('div[data-budget]');
    }).each(function (_, container) {
      $(container).parent().append(container);
    });
});

$('#main-content').on('click', '#sort-alpha-desc', function(e) {
    addHash($(this).prop('id'));
    $('div[data-budget] > div:first-child').sort(function (a, b) {
        var textA = $(a).find('a').text();
        var textB = $(b).find('a').text();
        return textB.localeCompare(textA);
    }).map(function() {
        return $(this).closest('div[data-budget]');
    }).each(function (_, container) {
      $(container).parent().append(container);
    });
});

if(window.location.hash.substr(1, 4) == 'sort') {
    $(window.location.hash).click();
}

function addHash(hash) {
    if(typeof history == 'object' && typeof history.replaceState == 'function') {
        history.replaceState(undefined, undefined, '#' + hash);
    } else {
        window.location.hash = hash;
    }
}
</script>
@endsection
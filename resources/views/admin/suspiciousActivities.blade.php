@extends('dashboard')

@section('content')
<div class="row">
    {!! Form::open() !!}
    <div class="col-xs-12">
        <div class="row" style="padding:10px 0; font-size:16px">
            <div class="col-xs-12">
                <strong>Sebelum / Sesudah sisiran</strong>: 
                Rp. {{ number_format($combedDiff[0] + $combedDiff[1] + $combedDiff[2], 2) }} /
                Rp. {{ number_format($combedDiff[0] + $combedDiff[2], 2) }} /
                {{ number_format($combedDiff[1] / ($combedDiff[0] + $combedDiff[1] + $combedDiff[2]) * 100, 2) }}%

            </div>
        </div>

        <div class="box">
            <div class="box-body table-responsive">
                <table class="table table-bordered table-striped">
                    <colgroup>
                        <col style="width: 8%"></col>
                        <col style="width: 5%"></col>
                        <col style="width: 36%"></col>
                        <col style="width: 18%"></col>
                        <col style="width: 18%"></col>
                        <col style="width: 15%"></col>
                    </colgroup>
                    <tr>
                        <th>Aksi</th>
                        <th>Flag</th>
                        @foreach($tableHeaders as $key => $header)
                        <th>
                            {{ $key }}
                            @if($key == Request::input('sort'))          
                                @if(Request::input('order') == 'asc')
                                <span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>
                                @else
                                <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                                @endif
                            @endif        
                        </th>
                        @endforeach
                    </tr>

                    @foreach($activities as $activity)
                        <tr>
                            <td>
                                <input type="radio" name="posts[{{ $activity->id }}]" id="post_{{ $activity->id }}_1" value="1" />
                                <label for="post_{{ $activity->id }}_1">Sisir</label> <br>

                                <input type="radio" name="posts[{{ $activity->id }}]" id="post_{{ $activity->id }}_2" value="2" />
                                <label for="post_{{ $activity->id }}_2">Aman</label> 
                            </td>
                            <td>
                                {{ $activity->flag_count }}
                            </td>
                            <td>
                                {!! Html::link('/activities/' . $activity->id, $activity->name) !!}
                            </td>
                            <td>
                                {{ $activity->skpd_name }}
                            </td>
                            <td>
                                Rp. {{ number_format($activity->budget_nominal) }}
                            </td>
                        </tr>
                    @endforeach
                </table>
                <br>
                {!! Form::submit() !!}
            </div>
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
    {!! Form::close() !!}
</div>
<!-- /.row -->
{!! $activities->appends(['sort' => Request::input('sort'), 'order' => Request::input('order')])->render() !!}
@endsection

@section('javascripts')
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=1033988359974181";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
@endsection
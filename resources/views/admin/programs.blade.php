@extends('dashboard');

@section('content')
    <ul>
    @foreach($programs as $program)
        <li>{!! Html::link('/committees/' . $committee_id . '/programs/' . $program->id . '/' . str_slug($program->name), $program->name) !!}</li>
    @endforeach
    </ul>
@endsection
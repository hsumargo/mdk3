@extends('dashboard')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">

            <div class="box-header">
              <h3 class="box-title">{{$pageDesc}}</h3>
            </div>

            <div class="box-body">
                <form class="form-inline">
                    <div class="form-group">
                        Cari:
                    </div>
                    <div class="form-group">
                        <input type="text" name="keyword" class="form-control col-md-3" id="order-search" placeholder="" value="{{ Input::get('keyword') }}">
                    </div>
                    <div class="form-group">
                        Filter By:
                    </div>
                    <div class="form-group">
                        {!! Form::select('filter', $filter, Input::get('filter'), array('class' => 'form-control col-md-2', 'id' => 'filter-activities')); !!}
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="activities" class="btn btn-info">Tampilkan Smua</a>
                </form>             
            </div>

            <div class="box-body table-responsive">
                <table class="table table-bordered table-striped">
                    <colgroup>
                        <col style="width: 33%"></col>
                        <col style="width: 33%"></col>
                        <col style="width: 33%"></col>
                    </colgroup>
                    <tr>
                        @foreach($tableHeaders as $key => $header)
                        <th>
                            {{ $key }}
                            @if($header == Request::input('sort'))
                                @if(Request::input('order') == 'asc')
                                <a href="{{ $url.'&sort='.$header.'&order='.'desc' }}" class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                                @elseif(Request::input('order') == 'desc')
                                <a href="{{ $url.'&sort='.$header.'&order='.'asc' }}" class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>
                                @endif
                            @else
                                <a href="{{ $url.'&sort='.$header.'&order='.'asc' }}" class="fa fa-sort" aria-hidden="true"></span>
                            @endif        
                        </th>
                        @endforeach
                    </tr>

                    @foreach($activities as $activity)
                        <tr>
                            <td>
                                {!! Html::link('/public/activities/' . $activity->id, $activity->name) !!}
                            </td>
                            <td>
                                {{ $activity->skpd_name }}
                            </td>
                            <td>
                                Rp. {{ number_format($activity->budget_nominal) }}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
{!! $activities->appends(['sort' => Request::input('sort'), 'order' => Request::input('order')])->render() !!}
@endsection

@section('javascripts')
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=1033988359974181";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
@endsection
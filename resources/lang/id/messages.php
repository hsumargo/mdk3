<?php

return array(

	"deleteCustomerSuccess"             	=> "Hapus data customer sukses",
	"deleteCustomerFailed"					=> "Hapus data customer gagal",
	"editCustomerSuccess"					=> "Edit data customer sukses",

	"addItemCategorySuccess"				=> "Tambah kategori barang sukses",
	"editItemCategorySuccess"				=> "Edit kategori barang sukses",
	"deleteItemCategoryFailed"				=> "Hapus kategori barang gagal",
	"deleteItemCategorySuccess"				=> "Hapus kategori barang sukses",

	"addOrderSuccess"						=> "Tambah purchase order sukses",
	"deleteOrderSuccess"             		=> "Hapus purchase order sukses",
	"deleteOrderFailed"						=> "Hapus purchase order gagal",
	"editOrderSuccess"						=> "Edit purchase order sukses",

);

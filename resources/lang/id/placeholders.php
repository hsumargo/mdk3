<?php

return array(

	"email"             	=> "Masukkan email",
	"name"					=> "Masukkan nama",
	"customerCode"			=> "Masukkan kode customer",
	"supplierCode"			=> "Masukkan kode supplier",
	"emailConfirmation"		=> "Konfirmasi email anda",
	"password"				=> "Enter password",
	"passwordConfirmation"	=> "Enter password confirmation",
	"address"				=> "Masukkan alamat",
	"postCode"				=> "Masukkan kode pos",
	"contactPerson"			=> "Masukkan contact person",
	"phone"					=> "Masukkan telepon",
	"notes"					=> "Masukkan notes",
	"itemCategoryName"		=> "Masukkan nama kategori barang",
	"orderCode"				=> "Masukkan kode order",
	"orderDate"				=> "Masukkan tanggal purchase order"

);

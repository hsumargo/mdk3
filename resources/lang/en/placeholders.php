<?php

return array(

	"email"             	=> "Masukkan email",
	"name"					=> "Masukkan nama",
	"emailConfirmation"		=> "Konfirmasi email anda",
	"password"				=> "Enter password",
	"passwordConfirmation"	=> "Enter password confirmation",
	"address"				=> "Masukkan alamat",
	"postCode"				=> "Masukkan kode pos",
	"contactPerson"			=> "Masukkan contact person",
	"phone"					=> "Masukkan telepon",
	"notes"					=> "Masukkan notes",

);

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => 'auth'], function () {
    Route::any('/suspiciousActivities', 'Dashboard\IndexController@suspiciousActivities');

    Route::get('/dashboard', 'Dashboard\IndexController@index');
});

/*
    Dashboard Routes
*/

//main dashboard
Route::get('/', 'Dashboard\IndexController@index');
//activities
Route::get('activities', 'Dashboard\IndexController@activities');

Route::get('commentsDetails', 'Dashboard\IndexController@getCommentsData');

Route::get('committees', 'Dashboard\IndexController@committees');
Route::get('committees/{id}/programs', 'Dashboard\IndexController@committees');
Route::get('committees/{id}/programs/{program_id}/{program_slug}', 'Dashboard\IndexController@committees');
Route::get('concernActivities', 'Dashboard\ActivityController@concernActivities');
Route::post('/flagPost/{id}', 'PublicController@flagPost');
Route::get('/activities/{id}', 'PublicController@activities');
Route::get('/budgetBrowser/activity/{id}', function($id) {
    return Redirect::to('/activities/' . $id, 301); 
});
Route::get('/naruto', 'PublicController@ninja');

Route::get('/budgetBrowser/', 'Dashboard\IndexController@budgetBrowser');
Route::get('/budgetBrowser/{objectName}/{id}', 'Dashboard\IndexController@budgetBrowser');
Route::get('/faq', 'Dashboard\IndexController@faq');


/*
    API Routes
*/
Route::get('/v1/skpd', ['uses' => 'API\SkpdController@index']);
Route::get('/v1/urusan', ['uses' => 'API\UrusanController@chart']);
Route::get('/v1/concern', ['uses' => 'API\ActivityController@concern']);
Route::get('/v1/concernActivities', ['uses' => 'API\ActivityController@concernActivities']);
Route::get('/v1/flagPost/{id}', ['uses' => 'API\FlagController@flagPost']);
Route::post('/v1/budgetComparisonPerMatter', ['uses' => 'API\ActivityController@budgetComparisonPerMatter']);
Route::get('/v1/activity/recalculateComments/{id}', ['uses' => 'API\ActivityController@recalculateComments']);

/*
    Auth Routes
*/
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');

Route::get('/facebook/login', function(SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb)
{
    // Send an array of permissions to request
    $login_url = $fb->getLoginUrl(['email']);
    return redirect($login_url . '&state=' . urlencode(URL::previous()));

    // Obviously you'd do this in blade :)
    // echo '<a href="' . $login_url . '">Login with Facebook</a>';
});

// Endpoint that is redirected to after an authentication attempt
Route::get('/facebook/callback', function(SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb)
{

    // Obtain an access token.
    try {
        $token = $fb->getAccessTokenFromRedirect();
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
        dd($e->getMessage());
    }

    // Access token will be null if the user denied the request
    // or if someone just hit this URL outside of the OAuth flow.
    if (! $token) {
        // Get the redirect helper
        $helper = $fb->getRedirectLoginHelper();

        if (! $helper->getError()) {
            abort(403, 'Unauthorized action.');
        }

        // User denied the request
        dd(
            $helper->getError(),
            $helper->getErrorCode(),
            $helper->getErrorReason(),
            $helper->getErrorDescription()
        );
    }

    if (! $token->isLongLived()) {
        // OAuth 2.0 client handler
        $oauth_client = $fb->getOAuth2Client();

        // Extend the access token.
        try {
            $token = $oauth_client->getLongLivedAccessToken($token);
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }
    }

    $fb->setDefaultAccessToken($token);

    // Save for later
    Session::put('fb_user_access_token', (string) $token);

    // Get basic info on the user from Facebook.
    try {
        $response = $fb->get('/me?fields=id,name,email,picture');
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
        dd($e->getMessage());
    }

    // Convert the response to a `Facebook/GraphNodes/GraphUser` collection
    $facebook_user = $response->getGraphUser();

    $user = new App\Models\User;

    $checkUser = App\Models\User::where('facebook_id', '=', $facebook_user['id'])->first();
    //store in database
    if(empty($checkUser)) {

        $status = $user->store($facebook_user);
        if($status === true) {

            $storedUser = App\Models\User::where('facebook_id', '=', $facebook_user['id'])->first();
            \Auth::loginUsingId('user', $storedUser->id);

            return redirect('/')->with('message', 'Successfully logged in with Facebook');
        } else {
            var_dump($status);
            die;
        }
    }

    \Auth::loginUsingId('user', $checkUser->id);

    $redirUrl = Request::url('/');
    if(Input::has('state')) {
        $redirUrl = Input::get('state');
    }
    return redirect($redirUrl)->with('message', 'Successfully logged in with Facebook');
});
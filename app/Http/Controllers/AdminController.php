<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AdminController extends BaseController 
{

	protected $data = [];
	protected $view = null;
	protected $parts = [];
	protected $mainTitle = 'Kawal APBD';
	protected $input = [];
	protected $adminUser = null;
	protected $breadCrumb = [];

	public function __construct(Request $request) 
	{
		$this->data['adminUser'] = Auth::user();
		$this->data['mainTitle'] = $this->mainTitle;
		if(Auth::user('admin')) {
			$this->data['mainTitle'] = $this->mainTitle . ' Dashboard';
		}

		$this->data['pageTitle'] = '';
		
		$this->data['navMenu'] = [
			[
				'title' => 'Publik',
				'icon' => 'fa fa-eye',
				'url' => '/'
			],
			[
				'title' => 'Dashboard',
				'icon' => 'fa fa-th',
				'url' => '/dashboard'
			],
			[
				'title' => 'Kegiatan',
				'icon' => 'glyphicon glyphicon-object-align-bottom',
				'activeUrls' => ['/activities', '/committees'],
				'children' => [
                    [
                        'title' => 'Semua Kegiatan',
                        'url'   => '/activities'
                    ],
                    [
                        'title' => 'Kegiatan Mencurigakan',
                        'url'   => '/suspiciousActivities'
                    ],
                    [
                    	'title' => 'Kegiatan Komisi',
                    	'url'	=> '/committees'
                    ]
                ]
			],
            
		];
		$this->data['pageDesc'] = '';

	}

	protected function response()
    {
        return view('admin.'.$this->view, $this->data);   
    }

}
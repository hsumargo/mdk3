<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Input;
use Validator;
use App\Exceptions\ApiException;
use App\Http\General\ApiInput;

class RestController extends BaseController 
{

	protected static $validation = [];

	public function __construct() 
	{

	}

	protected function validation() 
	{

		$method = ApiInput::controllerMethod();

		if(!isset(static::$validation[$method])) {
			return;
		}

		$validator = Validator::make(Input::all(), static::$validation[$method]);

        if ($validator->fails()) {
        	$failedParams = array_keys($validator->failed());
        	throw new ApiException(30000, ['params' => implode(', ', $failedParams)]);
        }

	}

    protected function success($check=null) 
    {

        if($check === null || $check) {
            return [
                'code' => 20000,
                'message' => 'Success.'
            ];
        }

        throw new ApiException;

    }

}

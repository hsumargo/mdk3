<?php

namespace App\Http\Controllers\API;

use Input;
use App\Http\Controllers\RestController;
use App\Exceptions\ApiException;
use App\Models\Skpd;
use App\Models\Activity;
use App\Models\Komisi;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;
use Html;
use Helper;
use DB;

class SkpdController extends RestController 
{

    protected static $validation = [
        'index' => [
        
        ]
    ];

    /**
     * @ApiDescription(section="Skpd", description="Mendapatkan list skpd yang ada di daerah")
     * @ApiMethod(type="get")
     * @ApiRoute(name="/skpd")
     * @ApiReturn(type="object")
     */
    public function index()
    {

        // $this->validation();
        $skpdModel = new Skpd;
        $skpd = $skpdModel->get(['sort' => 'skpd.name', 'limit' => Input::get('limit'), 'offset' => Input::get('offset')]);

        return $skpd;
    }
}
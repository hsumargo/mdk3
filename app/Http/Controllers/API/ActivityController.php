<?php

namespace App\Http\Controllers\API;

use Input;
use App\Http\Controllers\RestController;
use App\Exceptions\ApiException;
use App\Models\Skpd;
use App\Models\Activity;
use App\Models\Urusan;
use App\Models\Komisi;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;
use Html;
use Helper;
use DB;
use Cache;

class ActivityController extends RestController 
{

    protected static $validation = [
        'index' => [
        ]
    ];

    /**
     * @ApiDescription()
     * @ApiMethod(type="get")
     * @ApiRoute(name="/budgetDifferences")
     * @ApiReturn(type="array")
     */
    public function budgetDifferences()
    {

        $groupBy = [
            'committee_id' => 'Komisi',
            'skpd.id' => 'Nama SKPD',
            'program_id' => 'Nama Program'
        ];

        $columns = [
            'activity.name', 'activity.budget_nominal', 'activity.id', 'committee_id',
            'budget_nominal', 'program.name AS program_name',
            DB::raw('sum(budget_nominal) as budget_nominal'),
            DB::raw('skpd.name as skpd_name'),
        ];

        $inputs = Input::all();

        //Prepare the activities query, remember last order parameters and process them
        $activities = Activity::join('skpd', 'skpd.id', '=', 'activity.skpd_id')
            ->join('program', 'program.id', '=', 'activity.program_id')
            ->orderBy('budget_nominal', 'DESC')
            ->groupBy($inputs['groupBy'])
            ->select($columns);


        $komisi = new Komisi;
        $skpd = new Skpd;

        if($inputs['groupBy'] == 'skpd.id') {
            $inputs['filterData'] = $komisi->id($inputs['filterData']);
            $activities = $activities->where('committee_id', '=', $inputs['filterData']);
        } else if ($inputs['groupBy'] == 'program_id') {
            $inputs['filterData'] = $skpd->id($inputs['filterData']);
            $activities = $activities->where('skpd_id', '=', $inputs['filterData']);
        }

        $activities = $activities->get();

        $result = [];

        foreach($activities as $activity) {
            if($inputs['groupBy'] == 'skpd.id') {
                $result[] = [$activity->skpd_name, $activity->total_budget_banggar_final, $activity->total_budget_nominal];
            } else if($inputs['groupBy'] == 'program_id') {
                $result[] = [$activity->program_name, $activity->total_budget_banggar_final, $activity->total_budget_nominal];
            } else {
                $result[] = [$komisi->nama($activity->committee_id), $activity->total_budget_banggar_final, $activity->total_budget_nominal];
            }
        }

        return Response::json($result, 200, [], JSON_NUMERIC_CHECK);

    }

    /**
     * @api {get} /v1/concern/ 
     * 
     * @apiDescription Memberikan JSON Array yang berisikan data plot yang digunakan untuk google chart
     * <br><br><strong>1. Chart persentase anggaran per urusan dari total APBD</strong><br>
     * Biasanya untuk menggambar chart awal yang mengelompokkan presentase anggaran berdasarkan urusan, panggil endpoint 
     * <pre>/v1/concern?groupBy=urusan&filter=</pre>
     * 
     * Format data yang dikembalikan adalah sbb:
     * <pre>[["Urusan","Jumlah Budget"],["Pekerjaan Umum",10554391191066],["Kesehatan",6491777714740],["Perumahan Rakyat",4554075552719],["Pendidikan",3627151570579],["Otonomi Daerah. Pemerintahan Umum. Administrasi Keuangan Daerah. Perangkat Daerah. Kepegawaian. dan Persandian",3500952549769],["Lingkungan Hidup",2060964706190],["Energi dan Sumberdaya Mineral",1249173492499],["Perhubungan",583735162217],["Kebudayaan",381347941118],["Pemuda dan Olah Raga",317099051630],["Sosial",273480333165],["Komunikasi dan Informatika",169531623802],["Kelautan dan Perikanan",146950166606],["Ketahanan Pangan",90913953201],["Pariwisata",86766939851],["Kependudukan dan Catatan Sipil",79817209874],["Tenaga Kerja",75278743285],["Koperasi dan Usaha Kecil Menengah",52992896389],["Pertanian",52587565211],["Penataan Ruang",39224426632],["Perpustakaan",37298553006],["Pemberdayaan Masyarakat dan Desa",32937990466],["Keluarga Berencana dan Keluarga Sejahtera",23059659838],["Industri",22048451213],["Kesatuan Bangsa dan Politik Dalam Negeri",19533336738],["Perencanaan Pembangunan",18755368557],["Pemberdayaan Perempuan dan Perlindungan Anak",18202028207],["Penanaman Modal Daerah",10285433133],["Perdagangan",8946076446],["Kearsipan",3698138775],["Kehutanan",626513189],["Statistik",345256000]]</pre>
     *
     * <strong>2. Chart presentase pengelompokkan SKPD per urusan (misal: Pekerjaan Umum)</strong>
     * <pre>/v1/concern?groupBy=nama+skpd&filter=Pekerjaan+Umum</pre>
     * 
     * Format data yang dikembalikan adalah sbb:
     * <pre>[["Nama skpd","Jumlah Budget"],["Dinas Bina Marga",3187851317923],["Dinas Tata Air",2616566813197],["DINAS KEBERSIHAN",576079696768],["Unit Pengelola Sampah Terpadu",432077661582],["Unit Pelaksana Kebersihan Badan Air",399902892183],["Sudin Bina Marga - JAKBAR",364800542187],["Sudin Bina Marga - JAKSEL",342567637671],["Sudin Bina Marga - JAKUT",326126037408],["Sudin Bina Marga - JAKTIM",306278744808],["Sudin Tata Air - JAKUT",300905746255],["Sudin Tata Air - JAKBAR",250292596233],["Sudin Bina Marga - JAKPUS",174079646500],["Sudin Tata Air - JAKSEL",169789834348],["Sudin Kebersihan - JAKUT",167632505432],["Sudin Tata Air - JAKPUS",154569080948],["Sudin Tata Air - JAKTIM",146418378689],["Sudin Kebersihan - JAKBAR",132935973693],["Sudin Kebersihan - JAKSEL",129280132494],["Sudin Kebersihan - JAKTIM",125316914210],["Sudin Kebersihan - JAKPUS",120434096594],["Sudin Tata Air - Kep. Seribu",80745628862],["Sudin Kebersihan - Kep. Seribu",49302482874],["Sudin Bina Marga - Kep. Seribu",436830207]]</pre>
     *
     * <strong>3. Chart presentase pengelompokkan Program untuk satu SKPD (misal: SKPD Dinas Bina Marga)</strong>
     * <pre>/v1/concern?groupBy=program&filter=Dinas+Bina+Marga</pre>
     *
     * Format data yang dikembalikan adalah sbb:
     * <pre>[["Program","Jumlah Budget"],["Program Pembangunan\/Peningkatan Jalan dan Jembatan",2254925116628],["Program Pembebasan Tanah Untuk Pembangunan Prasarana dan Sarana ke-PU-an",522855980683],["Program Rehabilitasi\/Pemeliharaan Jalan dan Jembatan",304653874399],["Program Peningkatan Sarana dan Prasarana Pekerjaan Umum",55194414802],["Program Pembangunan, Peningkatan, dan Pemeliharaan Sarana Jaringan Utilitas",35000000000],["Program Peningkatan dan Pengelolaan Kantor Urusan Pekerjaan Umum",8612307286],["Program Perencanaan Pembangunan Daerah Tingkat SKPD Urusan Pekerjaan Umum",3250000000],["Program Pengelolaan Kendaraan Dinas Urusan Pekerjaan Umum",2068068480],["Program Pengembangan Data\/Informasi SKPD Urusan Pekerjaan Umum",1291555645]]</pre>
     *
     * @apiName Concern (Urusan)
     * @apiGroup Chart
     * @apiParam {String} groupBy grup data berdasarkan urusan / nama skpd / program
     * @apiParam {String} filter jika groupBy berdasarkan nama skpd, ini adalah id dari urusan, jika di grup berdasarkan program, ini adalah id dari skpd
     *
     * @apiParam {String} filterBy filter berdasarkan salah satu field: activity.name / skpd.name / budget_nominal / matter.name
     * @apiParam {String} keyword kata yang dicari di dalam kolom yang ditentukan oleh filterBy
     * @apiParam {String} callback nama callback yang digunakan untuk memanggil API dari JavaScript di host yang berbeda
     */
    public function concern()
    {
        $inputs = Input::all();

        $cacheKey = Helper::cacheKeyBuilder('concernAPI', $inputs);

        $result = Cache::remember($cacheKey, 3600, function() use ($inputs)
        {
            $columns = [
                'activity.name', 'activity.budget_nominal', 'activity.id', 'committee_id', 'matter.name as nama_urusan',
                DB::raw('sum(budget_nominal) as total_budget_nominal'),
                DB::raw('skpd.name as skpd_name'), DB::raw('program.name as program_name')
            ];

            $groupBy = ['urusan' => 'program.matter_id', 'nama skpd' => 'skpd.name', 'program' => 'program_name'];

            $orderBy = 'total_budget_nominal';

            $urusan = new Urusan;
            $skpd = new Skpd;

            $activities = Activity::join('skpd', 'skpd.id', '=', 'activity.skpd_id')
                            ->join('program', 'program.id', '=', 'activity.program_id')
                            ->join('matter', 'matter.id', '=', 'program.matter_id')
                            ->groupBy($groupBy[$inputs['groupBy']])
                            ->orderBy($orderBy, 'desc')
                            ->select($columns);

            if($inputs['groupBy'] == 'nama skpd') {
                $activities = $activities->where('matter.id', '=', $urusan->id($inputs['filter']));
            } elseif($inputs['groupBy'] == 'program') {
                $activities = $activities->where('skpd.id', '=', $skpd->id($inputs['filter']));
            }

            if(!empty($inputs['filterBy']) && !empty($inputs['keyword'])) {
                $activities = $activities->where($inputs['filterBy'], 'like', '%'.$inputs['keyword'].'%');
            }

            $activities = $activities->get();

            $result[] = [ucfirst($inputs['groupBy']), 'Jumlah Budget'];

            foreach($activities as $activity) {
                if($inputs['groupBy'] == 'urusan') {
                    $result[] = [$activity->nama_urusan, $activity->total_budget_nominal];
                } elseif($inputs['groupBy'] == 'nama skpd') {
                    $result[] = [$activity->skpd_name, $activity->total_budget_nominal];
                } elseif($inputs['groupBy'] == 'program') {
                    $result[] = [$activity->program_name, $activity->total_budget_nominal];
                }
            }

            return $result;

        });

        return Response::json($result, 200, [], JSON_NUMERIC_CHECK)->setCallback(Input::get('callback'));
    }

    /**
     * @ApiDescription()
     * @ApiMethod(type="get")
     * @ApiRoute(name="/budgetComparisonPerMatter")
     * @ApiReturn(type="array")
     */
    public function budgetComparisonPerMatter()
    {
        $inputs = Input::all();

        $groupBy = 'activity.matter_id';

        $budgetTotalPerGroup = Activity::leftJoin('matter', 'matter.id', '=', 'activity.matter_id')
                        ->leftjoin('program', 'program.id', '=', 'activity.program_id')
                        ->leftJoin('skpd', 'skpd.id', '=', 'activity.skpd_id')
                        ;

        $select = 'matter.name';

        $urusan = new Urusan;
        $skpd = new Skpd;

        if($inputs['groupBy'] == 'nama skpd') {
            $budgetTotalPerGroup = $budgetTotalPerGroup->where('matter.id', '=', $urusan->id($inputs['filter']));

            $select = 'skpd.name';
            $groupBy = 'skpd.id';
        } elseif($inputs['groupBy'] == 'program') {
            $budgetTotalPerGroup = $budgetTotalPerGroup->where('skpd.id', '=', $skpd->id($inputs['filter']));
            $select = 'program.name';
            $groupBy = 'program.id';
        }

        $budgetTotalPerGroup = $budgetTotalPerGroup
                                ->groupBy($groupBy)
                                ->orderBy('budgetPerGroup')
                                ->select([DB::raw('sum(budget_nominal) as budgetPerGroup'), $select])
                                ->lists('budgetPerGroup', $select);

        $results = [];
        unset($inputs['budget'][0]);

        foreach($inputs['budget'] as $budget) {
            $results[] = [$budget[0] . '( Rp ' . number_format($budget[1]) . ' / Rp ' . number_format($budgetTotalPerGroup[$budget[0]]) . ')', 
                            Helper::floorp($budget[1] / $budgetTotalPerGroup[$budget[0]] * 100, 2)];
        }

        return Response::json($results, 200, [], JSON_NUMERIC_CHECK);
    }

    public function recalculateComments($id)
    {
        $json = file_get_contents('https://graph.facebook.com/v2.5/?id=http://kawal-apbd.com/activities/' . $id . '&access_token=' . env('FACEBOOK_APP_TOKEN'));
        $json = json_decode($json);
        DB::table('activity')->where('id', $id)->update(array('comment_count' => $json->share->comment_count));
        Cache::flush();
    }
}
<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Request;
use App\Http\General\Helper;
use App\Models\Activity;
use App\Models\Komisi;
use App\Models\Program;
use App\Models\Skpd;
use Illuminate\Support\Str;
use DB;
use Input;
use Cache;
// use SammyK\LaravelFacebookSdk\SyncableGraphNodeTrait;
// use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;

// use Facebook\Facebook as Facebook;
// use Facebook\Exceptions\FacebookResponseException as FacebookResponseException;
// use Facebook\Exceptions\FacebookSDKException as FacebookSDKException;
// use Facebook\FacebookRequest as FacebookRequest;
// use Facebook\FacebookSession as FacebookSession;


class IndexController extends AdminController 
{

	public function index() 
    {
        $this->data['breadcrumb'] = [
            [
                'title' => 'Dashboard',
                'icon' => 'fa fa-th',
                'url' => '/dashboard'
            ]
        ];
        
        $this->data['pageTitle'] = '';
        $this->data['programs'] = ['' => 'Semua Program'] + DB::table('program')->lists('name', 'id');

        $this->view = 'index';
        return $this->response();
    }

    public function activities()
    {

        //Columns to retrieve from db
        $columns = [
            'activity.name', 'activity.id', 'committee_id', 'budget_nominal', 
            DB::raw('skpd.name as skpd_name')
        ];

        //Table headers to be rendered in the view
        $tableHeaders = [
            'Nama Kegiatan' => 'activity.name',
            'Nama SKPD' => 'skpd.name',
            'Anggaran' => 'budget_nominal'
        ];

        $filterBy = array_flip($tableHeaders);

        $data = Input::all();

        //Prepare the activities query, remember last order parameters and process them
        $activities = Activity::join('skpd', 'skpd.id', '=', 'activity.skpd_id');

        if(empty($data['sort'])) {
            $data['sort'] = 'activity.id';
        }

        if(empty($data['filter'])) {
            $data['filter'] = 'activity.name';
        }

        if(!empty($data['keyword']) && !in_array($data['sort'], $searchColumnExemption)) {
            $activities = $activities->where($data['filter'], 'like', '%'.$data['keyword'].'%');
        }

        if(empty($data['order'])  || !in_array($data['order'], ['desc', 'asc'])) {
            $data['order'] = 'asc';
        }

        $activities = $activities->orderBy($data['sort'], $data['order'])->paginate(50, $columns)->setPath('');

        $this->data = array_merge([
            'activities' => $activities,
            'tableHeaders' => $tableHeaders,
            'pageTitle' => 'Rancangan APBD Kegiatan 2016',
            'breadcrumb' =>  [
                [
                    'title' => 'List Kegiatan',
                    'icon' => 'fa fa-th',
                    'url' => '/'
                ]
            ],
            'sort' => array_flip($tableHeaders),
            'order' => Helper::order(),
            'url' => url('/activities?keyword='.Input::get('keyword').'&filter='.Input::get('filter')),
            'filter' => $filterBy
        ], $this->data);

        $this->view = 'activities';
        return $this->response();
    }

    public function suspiciousActivities()
    {
        if(Request::isMethod('post')) {
            $posts = Input::get('posts');
            foreach($posts as $postId => $sisir) {
                DB::table('activity')->where('id', $postId)->update(array('combed' => $sisir));   
            }

            return redirect()->action('Dashboard\IndexController@suspiciousActivities');
        }

        $this->data['combedDiff'] = Activity::select('combed', DB::raw('sum(budget_nominal) as sum'))->groupBy('combed')->lists('sum', 'combed');
        
        foreach([0,1,2] as $combed) {
            if(empty($this->data['combedDiff'][$combed])) {
                $this->data['combedDiff'][$combed] = 0;
            }
        }


        //Columns to retrieve from db
        $columns = [
            'activity.name', 'activity.budget_nominal', 'activity.id', 'committee_id', 
            DB::raw('count(flag.activity_id) as flag_count'),
            DB::raw('skpd.name as skpd_name'),
        ];

        //Table headers to be rendered in the view
        $tableHeaders = [
            'Nama Kegiatan' => 'activity.name',
            'Nama SKPD' => 'skpd.name',
            'Anggaran' => 'budget_nominal',
        ];

        $this->data['activities'] = Activity::join('skpd', 'skpd.id', '=', 'activity.skpd_id')
            ->select($columns)->join('flag', 'flag.activity_id', '=', 'activity.id')
            ->where('combed', '=', 0)->groupBy('activity.id')->orderBy('flag_count', 'DESC')
            ->paginate(50);

        $this->data['breadcrumb'] = [
            [
                'title' => 'Kegiatan Mencurigakan',
                'icon' => 'fa fa-th',
                'url' => '/'
            ]
        ];

        
        $this->data['pageTitle'] = 'Kegiatan paling mencurigakan menurut komunitas.';
        $this->data['tableHeaders'] = $tableHeaders;

        $this->view = 'suspiciousActivities';
        return $this->response();

    }

    /**
     *  Browse committee and their programs
     *  @param $id the committee id
     *  @param $program_id the program_id of the committee
     */

    public function committees($id = '', $program_id = '')
    {
        $view = $pageTitle = '';

        $breadcrumbs = [
            [
                'title' => 'Kegiatan per Komisi',
                'icon' => 'fa fa-th',
                'url' => '/'
            ],
            [
                'title' => 'Daftar Komisi',
                'url' => url('committees')
            ]
        ];

        //Breadcrumbs
        $committee_name = $program_name = '';
        if(!empty($id))
        {
            $committee_name = Komisi::find($id)->name;
            $breadcrumbs[] = [
                'title' => $committee_name,
                'url' => url('committees/' . $id . '/programs')
            ];
        }

        if(!empty($program_id))
        {
            $program_name = Program::find($program_id)->name;
            $breadcrumbs[] = [
                'title' => (strlen($program_name) > 100) ? substr($program_name, 0, 100).'...': substr($program_name, 0, 100) ,
                'url' => url('committees/' . $id . '/programs/' . $program_id . '/' . Str::slug($program_name))
            ];
        }

        //If both empty, returns all committees
        if(empty($id) && empty($program_id)) 
        {
            $this->data['committees'] = Komisi::all();
            $pageTitle = 'Daftar Komisi';
            $view = 'committees';
        } 
        //If id not empty and program_id is empty, list all programs of the committee
        else if(!empty($id) && is_numeric($id) && empty($program_id)) 
        {
            $this->data['programs'] = DB::table('activity')
                ->select(DB::raw('distinct program.id, program.name'))
                ->join('skpd', 'activity.skpd_id', '=', 'skpd.id')
                ->join('program', 'program.id', '=', 'activity.program_id')
                ->where('committee_id', '=', $id)
                ->get();
            $this->data['committee_id'] = $id;
            $view = 'programs';
            $pageTitle = 'Daftar Program ' . $committee_name;
        }
        //List of activities on the program
        else if(!empty($id) && !empty($program_id)) 
        {
            //Table headers to be rendered in the view
            $tableHeaders = [
                'Nama Kegiatan' => 'activity.name',
                'Nama SKPD' => 'skpd.name',
                'Anggaran' => 'budget_nominal',
            ];

            $filterBy = array_flip($tableHeaders);
            unset($filterBy['budget_delta']);

            $searchColumnExemption = ['budget_delta'];

            $data = Input::all();

            //Prepare the activities query, remember last order parameters and process them
            $activities = Activity::where('program_id', '=', $program_id);

            if(empty($data['sort'])) {
                $data['sort'] = 'activity.id';
            }

            if(!empty($data['keyword']) && !in_array($data['sort'], $searchColumnExemption)) {
                $activities = $activities->where('activity.name', 'like', '%'.$data['keyword'].'%');
            }

            if(empty($data['order'])  || !in_array($data['order'], ['desc', 'asc'])) {
                $data['order'] = 'asc';
            }

            $this->data['activities'] = $activities
                ->select('activity.id', 'activity.name', 'skpd.name AS skpd_name', 'program.name AS program_name', 'budget_nominal')
                ->join('skpd', 'skpd.id', '=', 'activity.skpd_id')
                ->join('program', 'program.id', '=', 'activity.program_id')
                ->where('program_id', '=', $program_id)
                ->orderBy($data['sort'], $data['order'])->paginate(50)->setPath('');

            $this->data['url'] = url('/committees/'.$id.'/programs/'.$program_id.'/'.$program_name.'?keyword='.Input::get('keyword').'&filter='.Input::get('filter'));
            $this->data['tableHeaders'] = $tableHeaders;
            $this->data['sort'] = array_flip($tableHeaders);
            $this->data['order'] = Helper::order();
            $this->data['filter'] = $filterBy;

            $view = 'activities';
            $this->data['pageDesc'] = 'Daftar Kegiatan ' . $program_name;
        }

        $this->view = $view;
        $this->data['pageTitle'] = $pageTitle;
        $this->data['breadcrumb'] = $breadcrumbs;

        return $this->response();
    }

    public function budgetBrowser($objectName = '', $parentId = 0)
    {

        $this->view = 'budgetBrowser';
        $pageTitle = '';

        if(empty($objectName)) {
            $objectName = 'committee';
        }

        $objects = array();
        if($objectName == 'committee') {
            if($parentId == 0) {
                $pageTitle = 'Anggaran per komisi';
                $objects = Cache::rememberForever('committees_budget', function() {
                    return DB::table('activity')
                        ->join('skpd', 'skpd.id', '=', 'activity.skpd_id')
                        ->join('committee', 'skpd.committee_id', '=', 'committee.id')
                        ->groupBy('committee.id')
                        ->orderBy('budget', 'DESC')
                        ->select([
                            'committee.id', 'committee.name', DB::raw('sum(budget_nominal) as budget')
                        ])->get(); 
                });
            } else {
                $pageTitle = 'Anggaran per SKPD dari ' . Komisi::nama($parentId);
                $objectName = 'skpd';
                $objects = Cache::rememberForever('skpds_budget_' . $parentId, function() use ($parentId) {
                    return DB::table('activity')
                        ->join('skpd', 'skpd.id', '=', 'activity.skpd_id')
                        ->where('skpd.committee_id', $parentId)
                        ->groupBy('skpd.id')
                        ->orderBy('budget', 'DESC')
                        ->select([
                            'skpd.id', 'skpd.name', DB::raw('sum(budget_nominal) as budget')
                    ])->get(); 
                });

            }
        } else if($objectName == 'skpd') {
            $pageTitle = 'Anggaran per kegiatan dari SKPD ' . Skpd::nama($parentId);
            $objects = Cache::rememberForever('activity_budget_' . $parentId, function() use ($parentId) {
                return DB::table('activity')
                    ->join('skpd', 'skpd.id', '=', 'activity.skpd_id')
                    ->where('activity.skpd_id', $parentId)
                    ->orderBy('budget', 'DESC')
                    ->select([
                        'activity.id', 'activity.name', DB::raw('budget_nominal as budget')
                    ])->get(); 
            });
            $objectName = 'activity';
        }
        
        $this->data['pageTitle'] = $pageTitle;

        $budgetMax = $budgetTotal = 0;
        foreach($objects as $object) {
            $budgetTotal += $object->budget;
            if($object->budget > $budgetMax) {
                $budgetMax = $object->budget;
            }
        }

        $this->data = array_merge([
            'budgetTotal' => $budgetTotal,
            'budgetMax' => $budgetMax,
            'objects' => $objects,
            'objectName' => $objectName,
        ], $this->data);

        return $this->response();
    }

    public function faq()
    {
        $this->data['pageTitle'] = '';
        $this->view = 'faq';
        return $this->response();
    }
}

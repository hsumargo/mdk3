<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Request;
use App\Http\General\Helper;
use App\Models\Activity;
use App\Models\Komisi;
use App\Models\Program;
use App\Models\Skpd;
use App\Models\Urusan;
use Illuminate\Support\Str;
use DB;
use Input;
use Cache;


class ActivityController extends AdminController 
{

    /**
     * @api {get} /concernActivities
     * @apiDescription Endpoint ini mengembalikan tabel yang berisikan kegiatan berdasarkan urusan / skpd / program yang diminta melalui param, biasanya dipanggil bersamaan dengan /v1/concern, dengan key=value yang sama.
     * Untuk dokumentasi lengkap, silakan merujuk ke <a href="#api-Chart-Concern__Urusan_">/v1/concern</a>
     *
     * Endpoint ini mengembalikan tabel yang berisikan data kegiatan berupa html ke client, data akan dipaginasi, halaman dapat diminta dengan parameter page=<halaman>
     * @apiName Concern Activities (kegiatan)
     * @apiGroup Tabel
     * @apiParam {String} groupBy grup data berdasarkan urusan / nama skpd / program
     * @apiParam {String} filter jika groupBy berdasarkan nama skpd, ini adalah id dari urusan, jika di grup berdasarkan program, ini adalah id dari skpd
     *
     * @apiParam {String} filterBy filter berdasarkan salah satu field: activity.name / skpd.name / budget_nominal / matter.name
     * @apiParam {String} keyword kata yang dicari di dalam kolom yang ditentukan oleh filterBy
     * @apiParam {Integer} page nomor halaman yang diminta
     * @apiParam {String} callback nama callback yang digunakan untuk memanggil API dari JavaScript di host yang berbeda
     */
	public function concernActivities() 
    {
    	$inputs = Input::all();
        $cacheKey = Helper::cacheKeyBuilder('concernActivities', $inputs);
        $data = Cache::remember($cacheKey, 3600, function() use ($inputs)
        {

            $columns = [
                'activity.*', 'committee_id', 'matter.name as nama_urusan', 'skpd.name as skpd_name', 
                'budget_nominal', DB::raw('count(flag.id) as flag_count')
            ];

            $groupBy = ['urusan' => 'program.matter_id', 'nama skpd' => 'skpd.name', 'program' => 'program_name'];
            $orderBy = ['Nama Kegiatan' => 'activity.name', 'urusan' => 'nama_urusan', 'nama skpd' => 'skpd.name', 'program' => 'program_name'];

            $tableHeaders =  [
                'Nama Kegiatan' => 'activity.name',
                'Nama SKPD' => 'skpd.name',
                'Anggaran' => 'budget_nominal',
                'Urusan' => 'matter.name'
            ];

            $orderable = array_merge($tableHeaders, ['flag_count' => 'flag_count', 'comment_count' => 'comment_count']);

            $filterBy = array_flip($tableHeaders);

            $order = 'desc';
            $orderBy = 'comment_count';
            if(!empty($inputs['orderBy'])) {
                $orderBy = $orderable[$inputs['orderBy']];
            }

            if(!empty($inputs['order'])) {
                $order = $inputs['order'];
            }

            //Prepare the activities query, remember last order parameters and process them
            $activities = Activity::join('skpd', 'skpd.id', '=', 'activity.skpd_id')
                ->join('program', 'program.id', '=', 'activity.program_id')
                ->join('matter', 'matter.id', '=', 'program.matter_id')
                ->join('flag', 'flag.activity_id', '=', 'activity.id', 'left outer')
                ->groupBy('activity.id')
                ->orderBy($orderBy, $order)
                ->select($columns);

            $urusan = new Urusan;
            $skpd = new Skpd;

            if($inputs['groupBy'] == 'nama skpd') {
                $activities = $activities->where('matter.id', '=', $urusan->id($inputs['filter']));
            } elseif($inputs['groupBy'] == 'program') {
                $activities = $activities->where('skpd.id', '=', $skpd->id($inputs['filter']));
            }

            if(!empty($inputs['filterBy']) && !empty($inputs['keyword'])) {
                $activities = $activities->where($inputs['filterBy'], 'like', '%'.$inputs['keyword'].'%');
            }

            return array_merge([
                'activities' => $activities->paginate(50, $columns)->setPath(Request::url('/concernActivities')), 
                'tableHeaders' => $tableHeaders,
                'sort' => array_flip($tableHeaders),
                'filterBy' => $filterBy,
                'orderColumn' => (strtolower($order) == 'asc') ? 'desc' : 'asc',
                'sortColumn' => $orderBy,
            ], $this->data);
        });

        $this->data = $data;
        $this->view = 'blocks.activitiesConcern';

        if(Input::has('callback')) 
        {
            return Response::json(['table' => view('admin.blocks.activitiesConcern', $this->data)->render()], 200, [], JSON_NUMERIC_CHECK)->setCallback(Input::get('callback'));
	
        } else 
        {
            return $this->response();
        }
        
    }

}

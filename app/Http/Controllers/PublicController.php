<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Activity;
use App\Models\Skpd;
use App\Models\User;
use App\Models\Flag;
use DB;
use Helper;
use Input;
use Cache;

class PublicController extends AdminController 
{
    protected $data = [];
    protected $view = null;
    protected $mainTitle = 'Kawal APBD';

    public function index() 
    {
        $this->view = 'index';

        $activities = Activity::leftJoin('flag', 'flag.activity_id', '=', 'activity.id', 'outer')
                        ->groupBy('activity.id')
                        ->select('activity.id', 'budget_nominal', 'activity.name', 
                            'activity.skpd_id', DB::raw('count(flag.id) as count'))
                        ->orderBy('id', 'DESC');

        if(Input::has('search')) {
            $activities->where('name', 'like', '%' . Input::get('search') . '%');
        }

        $activities = $activities->paginate(30);
        $skpd = new Skpd;

        $activityIds = [];
        foreach($activities as $i => $activity) {
            $activityIds[] = $activity->id;
            $activities[$i]['flag_count'] = $activity->flags()->count();
            $activities[$i]['skpd_name'] = $skpd->nama($activity->skpd_id);
        }


        if(Auth::check()) {
            $this->data['flags'] = Flag::where('user_id', '=', Auth::user()->id)->whereIn('activity_id', $activityIds)->lists('id', 'activity_id');
        }

        $this->data['activities'] = $activities;

        return $this->response();
    }

    public function activities($id) 
    {
        $this->view = 'activity';

        $cacheKey = Helper::cacheKeyBuilder('activityDetails', $id);

        $activity = Cache::remember($cacheKey, 3600, function() use ($id)
        {
            return Activity::find($id);
        });

        $this->data['activity'] = $activity;
        $flag = Flag::where('activity_id', '=', $activity->id);

        if(Auth::check()) {
            $flag->where('user_id', '=', Auth::user()->id);
            $this->data['isFlagged'] = $flag->count() >= 1;
        } else {
            $this->data['isFlagged'] = false;
        }

        $this->mainTitle = 'Kawal APBD - ' . $activity->name;

        return $this->response();
    }

    public function ninja() 
    {
        $activities = Activity::all();
        foreach($activities as $activity) {
            $url = urlencode('http://kawal-apbd.com/activities/' . $activity->id);
            file_get_contents('https://graph.facebook.com/?id=' . $url . '&scrape=true');
        }
    }

    public function login($id)
    {
        Auth::login(User::find($id));
        Helper::pr(Auth::user());
    }

	protected function response()
    {
        $this->data['mainTitle'] = $this->mainTitle;
        return view('public.'.$this->view, $this->data);
    }

    /** Flag a post
     *  @param $id
     */
    public function flagPost($id) 
    {
        if(empty(Auth::user('user'))) {
            return response()->json(['error' => 'invalid user', 'code' => 11]);
        }

        $flagRecord = DB::table('flag')->where('user_id', '=', Auth::user('user')->id)->where('activity_id', '=', $id)->first();

        if(Input::get('flag') == "false") {

            if(empty($flagRecord)) {
                return response()->json(['error' => 'flag record not found', 'code' => 12]);
            }

            DB::table('flag')->where('user_id', '=', Auth::user('user')->id)->where('activity_id', '=', $id)->delete();

        } elseif(Input::get('flag') == "true") {

            if(empty($flagRecord)) {
                DB::insert('insert into flag (user_id, activity_id) values (?, ?)', [Auth::user('user')->id, $id]);
            }
            
        } else {
            return response()->json(['error' => 'invalid input', 'code' => 13]);
        }

        Cache::flush();
        return response()->json(['success' => true, 'flag_count' => DB::table('flag')->where('activity_id', '=', $id)->count()]);
    }
}
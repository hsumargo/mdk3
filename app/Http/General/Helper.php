<?php

namespace App\Http\General;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Helper {

	public static function defaultTimestamp($input=null) 
    {

		if(empty($input)) {
			$input = time();
		}
		else if((int)$input == 0) {
			$input = strtotime($input);
		}

		return date('Y-m-d H:i:s', $input);

	}

	public static function isValidEmail($string) 
	{
		return (filter_var($string, FILTER_VALIDATE_EMAIL));
	}

	public static function userDateFromTime($time) 
	{
		return date('Ymd', $time);
	}

    public static $kk = ['Triliun', 'Miliar', 'Juta', 'Ribu'];
    public static $kkN = [1000000000000, 1000000000, 1000000, 1000];
    public static function shortenNumber($input) 
    {
        $input = abs($input);
        
        if($input < 1000) {
            return '<span class="diff-nominal">' . $input . '</span><br><span class="diff-kelipatan"> </span>';
        }

        foreach(Helper::$kkN as $i => $number) {
            if($input < $number) {
                continue;
            } else {
                return '<span class="diff-nominal">' . (floor($input / $number * 10) / 10). '</span><br><span class="diff-kelipatan">' . Helper::$kk[$i] . '</span>';
            }
        }

        return $input;
    }

	public static function httpRequest($url, $params, $method = 'POST', $jsonResponse=false) 
	{

        $c = curl_init();
        curl_setopt($c, CURLOPT_TIMEOUT, 5);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLINFO_HEADER_OUT, true);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST , false);
        curl_setopt($c, CURLOPT_CUSTOMREQUEST, $method);

        if($method == 'PUT' || $method == 'POST'){
            curl_setopt($c, CURLOPT_POSTFIELDS, $params);
        } elseif($method == 'GET') {
            $url = $url.'?'.http_build_query($params);
        }

        curl_setopt($c, CURLOPT_URL, $url);

        $response = curl_exec($c);
        $responseCode = curl_getinfo($c, CURLINFO_HTTP_CODE);

        if($jsonResponse) {
        	return json_decode($response);
    	}
    	else {
    		return $response;
    	}

    }

    public static function pr() 
    {
        $vars = func_get_args();

        if(count($vars) > 0) {
            echo '<pre>',PHP_EOL;
            foreach($vars as $v) {
                echo print_r($v, 1), PHP_EOL;
            }
            echo '</pre>',PHP_EOL;
        }
    }

    public static function rupiah_format($input)
    {
        return 'Rp. ' . number_format(floatval($input), 2);
    }

    public static function rupiah_long_format($input)
    {
        return number_format(floatval($input), 2) . ' rupiah';
    }

    public static function createList($arr, $urutan) 
    {
        if($urutan==0){
            $html = "\n<ul class='sidebar-menu'>\n";
        } else {
            $html = "\n<ul class='treeview-menu'>\n";
        }

        foreach ($arr as $key=>$v)
        {
            $activeClass = "";
            if (array_key_exists('activeUrls', $v)) {
                $activeClass = in_array($_SERVER['REQUEST_URI'], $v['activeUrls']) ? 'active' : '';
            }

            if (array_key_exists('children', $v)) {

                
                $html .= "<li class='treeview ".$activeClass."'>\n";
                $html .= '<a href="#">
                                <i class="'.$v['icon'].'"></i>
                                <span>'.$v['title'].'</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>';
 
                $html .= static::createList($v['children'],1);
                $html .= "</li>\n";
            } else{
                $activeClass = ($v['url'] == $_SERVER['REQUEST_URI']) ? 'active' : '';
                $html .= '<li class="'.$activeClass.'"><a href="'.$v['url'].'">';
                if($urutan==0) {
                    $html .=    '<i class="'.$v['icon'].'"></i>';
                }
                if($urutan==1) {
                    $html .=    '<i class="fa fa-angle-double-right"></i>';
                }
                $html .= $v['title']."</a></li>\n";
            }
        }
        $html .= "</ul>\n";
        return $html;
    }

    public static function createBreadcrumb($arr) 
    {
        $html = "\n<ol class='breadcrumb'>\n";
       
        foreach ($arr as $key=>$v)
        {
            $html .= '<li><a href="'.$v['url'].'">';

            if(!empty($v['icon'])) 
            {
                $html .= '<i class="'.$v['icon'].'"></i>';   
            }

            $html .= $v['title']."</a></li>\n";
        }
        $html .= "</ol>\n";
        return $html;
    }

    public static function isAdmin()
    {   
        $user = Auth::user();

        if(!empty($user)) {
            if($user->role == 99) {
                return true;
            }
        }

        return false;
    }

    public static function order()
    {
        return ['asc' => 'Asc', 'desc' => 'Desc'];
    }

    public static function floorp($val, $precision)
    {
        $mult = pow(10, $precision);
        return floor($val * $mult) / $mult;
    }

    public static function displayPriceDetails($komponen) 
    {
        for($i = 1; $i <= 5; $i++) {
            if($komponen->{'vol' . $i} > 0) 
            {
                if($i > 1) 
                {
                    echo ', ';
                }
                echo $komponen->{'vol' . $i} . ' ' . $komponen->{'unit' . $i};
            }
        }

        echo ' @' . Helper::rupiah_format($komponen->component_budget_per_unit);

        if($komponen->component_tax > 0) 
        {
            echo '<br>Pajak: ' . Helper::rupiah_format($komponen->component_tax);
        }
    }

    public static function cacheKeyBuilder($tag, $data = [])
    {
        return $tag . md5(serialize($data));
    }
}

<?php

namespace App\Http\General;

use Request;
use Input;
use Helper;

class ApiInput 
{

	public static function verb() 
	{
		return Request::method();
	}

	public static function deviceType() 
	{
		return Input::get('deviceType');
	}

	public static function version() 
	{
		return Input::get('version');
	}

	public static function checkVersion($ios, $android=null) 
	{
        return true;   
    }

	private static $controllerTag = null;
	private static $controllerInfo = null;

	public static function controllerTag() 
	{

		if(static::$controllerTag !== null) {
			return static::$controllerTag;
		}

		$route = Request::route()->getAction();

		if(isset($route['uses'])) {
			static::$controllerTag = str_replace('App\Http\Controllers\API\\', '', $route['uses']);
		}
		else {
			static::$controllerTag = '';
		}

		return static::$controllerTag;

	}

	private static function controllerInfo() 
	{

		if(static::$controllerInfo !== null) {
			return static::$controllerInfo;
		}

		$info = explode('@', static::controllerTag());
		
		static::$controllerInfo = [
			'class' => $info[0],
			'method' => $info[1]
		];
		
		return static::$controllerInfo;

	}

	public static function controllerClass() 
	{
		$info = static::controllerInfo();
		if($info !== null) {
			return $info['class'];
		}
	}

	public static function controllerMethod() 
	{
		$info = static::controllerInfo();
		if($info !== null) {
			return $info['method'];
		}
	}

}
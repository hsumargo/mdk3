<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;
use Hash;
use DB;
use Input;
use Cache;

use Illuminate\Foundation\Validation\ValidatesRequests;

final class Skpd extends Base
{
    use ValidatesRequests;

	protected $table = 'skpd';

    protected $fillable = [
        'committee_id', 'name', 'createdAt', 'updatedAt'
    ];

    protected static $rules = [
        'committee_id' => 'required',
        'name' => 'required|unique:skpd',
    ];

    protected $sortColumns = [
        'name' => 'Nama',
        'committee_id' => 'Komisi',
    ];

    protected $selectColumns = [
        'skpd.id',
        'committee.name as komisi',
        'skpd.name',
    ];

    protected $perPage = 50;
    protected $offset = 0;

    public function get($data = []) 
    {
        if(!empty($data['limit'])) {
            $this->perPage = $data['limit'];
        }

        // if(!empty($data['offset'])) {
        //     $this->offset = $data['offset'];
        // }

        // return $this->buildCustomQuery($data)->take($this->perPage)->skip($this->offset)->get();

    	// return $this->buildCustomQuery($data)->paginate($this->perPage)->items();
        return $this->buildCustomQuery($data)->paginate($this->perPage);
    }

    public function count($data = []) 
    {
        return $this->buildCustomQuery($data)->count();
    }

    public function buildCustomQuery($data = []) 
    {
        $query = $this->select($this->selectColumns);

        if(empty($data['sort']) || !in_array($data['sort'], $this->fillable)) {
            $data['sort'] = 'id';
        }

        $query = $query->leftJoin('committee', 'committee.id', '=', 'skpd.committee_id');

        if(empty($data['order'])  || !in_array($data['sort'], ['desc', 'asc'])) {
            $data['order'] = 'asc';
        }

        return $query->orderBy($data['sort'], $data['order']);
    }

    public function store(Array $attributes=[])
    {

        $validator = Validator::make($attributes, static::$rules);
        if($validator->fails()) {
            return $validator->errors()->all();
        }

        // Start transaction!
        DB::beginTransaction();

        try {
            $skpd = Skpd::create($attributes);
        } catch(ValidationException $e) {
            // Rollback and then redirect
            // back to form with errors
            DB::rollback();
            return false;
        } catch(\Exception $e) {
            DB::rollback();
            return false;
        }

        // If we reach here, then
        // data is valid and working.
        // Commit the queries!
        DB::commit();

        return true;
    }

    public function updateEntry(Array $attributes=[])
    {
        // $validator = Validator::make($attributes, static::$rules);
        // if($validator->fails()) {
        //     return $validator->errors()->all();
        // }

        if($this->fill($attributes)->save()) {
            return true;
        }

        return false;
    }

    public static function nama($id)
    {
        $nama = Cache::rememberForever('nama_skpd_'.$id, function() use ($id) 
        {
            $skpd = DB::table('skpd')->where('id', '=', $id)->select('name')->first();
            return $skpd->name;
        });

        return $nama;
    }

    public function id($name)
    {
        $id = Cache::rememberForever('id_skpd_'.$name, function() use ($name) 
        {
            $skpd = DB::table('skpd')->where('name', '=', $name)->select('id')->first();
            return $skpd->id;
        });

        return $id;
    }

}

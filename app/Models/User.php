<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use DB;
use Input;
use Helper;

use Illuminate\Foundation\Validation\ValidatesRequests;

final class User extends Base implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

	protected $table = 'user';

    protected $fillable = [
        'id', 'facebook_id', 'email', 'fullname', 'profile_pic', 'createdAt', 'updatedAt'
    ];

    protected static $rules = [
        'facebook_id' => 'required|unique:user',
        'email' => 'required',
        'fullname'  => 'required',
        'profile_pic' => 'required',
    ];

    protected $sortColumns = [

    ];

    protected $selectColumns = [
        
    ];

    public function store($attributes)
    {

        $data['facebook_id'] = $attributes['id'];
        $data['fullname'] = $attributes['name'];
        $data['email'] = $attributes['email'];
        $data['profile_pic'] = $attributes['picture']['url'];

        $validator = Validator::make($data, static::$rules);
        if($validator->fails()) {
            return $validator->errors()->all();
        }

        // Start transaction!
        DB::beginTransaction();

        try {
            $user = User::create($data);
        } catch(ValidationException $e) {
            // Rollback and then redirect
            // back to form with errors
            DB::rollback();
            return false;
        } catch(\Exception $e) {
            DB::rollback();
            return false;
        }

        // If we reach here, then
        // data is valid and working.
        // Commit the queries!
        DB::commit();

        return true;
    }

}

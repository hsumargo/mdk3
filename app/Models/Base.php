<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use DB;
use Input;
use Illuminate\Foundation\Validation\ValidatesRequests;

class Base extends Model {

	// const CREATED_AT = 'createdAt';
 //    const UPDATED_AT = 'updatedAt';

    protected $sortColumns = [];

	public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

    public function sortColumns() 
    {
        return $this->sortColumns;
    }

	public static function boot() {

        parent::boot();

        static::saving(function($model) {

    	});

        static::saved(function($model) {

        });

        static::deleted(function($model) {

        });

    }

    protected static function uploadAssets($model) {

    	foreach($model::$assets as $field) {
    		if(Assets::hasFile($field)) {
    			$model->$field = Assets::upload($field, static::tableName().'.'.$field);
    		}
    	}

    }

	public function fill(array $attributes) {
        return parent::fill($attributes);
    }

    public function hasGetMutator($key) {
        return parent::hasGetMutator($key);
    }

    protected function mutateAttribute($key, $value) {
        return parent::mutateAttribute($key, $value);
    }

    public static function tableName() {
        return (new static)->getTable();
    }

}

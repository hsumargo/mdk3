<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;
use Hash;
use DB;
use Input;

use Illuminate\Foundation\Validation\ValidatesRequests;

final class Flag extends Base
{
    use ValidatesRequests;

	protected $table = 'flag';

    protected $fillable = [
        'id', 'activity_id', 'user_id'
    ];

    protected static $rules = [

    ];

    protected $sortColumns = [
    
    ];

    protected $selectColumns = [
        
    ];

    public function activity() {
        return $this->belongsTo('App\Models\Activity');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

}

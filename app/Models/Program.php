<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;
use Hash;
use DB;
use Input;

use Illuminate\Foundation\Validation\ValidatesRequests;

final class Program extends Base
{
    use ValidatesRequests;

	protected $table = 'program';

    protected $fillable = [
        'id', 'matter_id', 'num', 'name', 'createdAt', 'updatedAt'
    ];

    protected static $rules = [

    ];

    protected $sortColumns = [

    ];

    protected $selectColumns = [
        
    ];

    protected $perPage = 50;
    protected $offset = 0;

    public function get($data = []) 
    {
        if(!empty($data['limit'])) {
            $this->perPage = $data['limit'];
        }

        if(!empty($data['offset'])) {
            $this->offset = $data['offset'];
        }

        return $this->buildCustomQuery($data)->take($this->perPage)->skip($this->offset)->get();
    }

    public function count($data = []) 
    {
        return $this->buildCustomQuery($data)->count();
    }

    public function buildCustomQuery($data = []) 
    {
        $query = $this->select($this->selectColumns);

        if(empty($data['sort']) || !in_array($data['sort'], $this->fillable)) {
            $data['sort'] = 'id';
        }

        if(empty($data['order'])  || !in_array($data['sort'], ['desc', 'asc'])) {
            $data['order'] = 'asc';
        }

        return $query->orderBy($data['sort'], $data['order']);
    }

    public function store(Array $attributes=[])
    {

        $validator = Validator::make($attributes, static::$rules);
        if($validator->fails()) {
            return $validator->errors()->all();
        }

        // Start transaction!
        DB::beginTransaction();

        try {
            $program = Program::create($attributes);
        } catch(ValidationException $e) {
            // Rollback and then redirect
            // back to form with errors
            DB::rollback();
            return false;
        } catch(\Exception $e) {
            DB::rollback();
            return false;
        }

        // If we reach here, then
        // data is valid and working.
        // Commit the queries!
        DB::commit();

        return true;
    }

    public function updateEntry(Array $attributes=[])
    {

        if($this->fill($attributes)->save()) {
            return true;
        }

        return false;
    }
}

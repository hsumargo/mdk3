<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Http\General\ApiInput;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        'Symfony\Component\HttpKernel\Exception\HttpException'
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {

        if(env('APP_ENV') != 'local') {

            if(!($e instanceof ApiException)) {
                $e = new ApiException();
            }

            $headerStatusCode = $e->getStatusCode();
            if(in_array(ApiInput::deviceType(), ['ios', 'android'])) {
                $headerStatusCode = 200;
            }

            return response(json_encode([
                'status' => $e->getStatusCode(),
                'code' => $e->getApiCode(),
                'error' => $e->getMessage()
            ]), $headerStatusCode);

        }

        return parent::render($request, $e);
    }
}

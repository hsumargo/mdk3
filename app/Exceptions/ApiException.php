<?php

namespace App\Exceptions;

use Config;

class ApiException extends \RuntimeException 
{

    private $apiCode;
    private $messageParams;
    private $statusCode;

    public function __construct($apiCode=30000, $messageParams=array(), $message = null, \Exception $previous = null, array $headers = array(), $code = 0)
    {

        $this->messageParams = $messageParams;
        $this->headers = $headers;
        
        if(!is_string($apiCode)) {
            //Get error details based on apiCode
            $message = trans('apiexceptions.'.$apiCode, $this->messageParams);
        }
        else {
            $message = $apiCode;
            $apiCode = 30000;
        }

        $this->apiCode = $apiCode;
        $this->statusCode = (int)Config::get('apiexceptions.'.$apiCode.'.status');

        parent::__construct($message, $code, $previous);
    }

    public function getApiCode()
    {
        return $this->apiCode;
    }

    public function getMessageParams() {
        return $this->messageParams;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getHeaders()
    {
        return $this->headers;
    }
}

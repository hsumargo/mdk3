<?php

namespace App\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;

class CustomValidationProvider extends ServiceProvider {

    public function register() { }

    public function boot() {

        Validator::extend('validUsername', function($attribute, $value, $parameters) {
            return $this->validUsername($attribute, $value, $parameters);
        });

    }

    public function validUsername($attribute, $value, $parameters) {

        $validator = Validator::make([$value], ['min:5|max:30']);

        if($validator->fails()) {
            return false;
        }

        return ctype_alnum(str_replace(['_'], '', $value));

    }


}
